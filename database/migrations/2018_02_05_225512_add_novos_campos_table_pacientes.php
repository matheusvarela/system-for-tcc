<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNovosCamposTablePacientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pacientes', function (Blueprint $table) {
            //
            $table->string('endereco', 100)->default('');
            $table->string('cep', 8)->default('');
            $table->string('referencia', 100)->default('');
            $table->string('profissao', 30)->default('');
            $table->string('local_trabalho', 30)->default('');
            $table->string('renda', 30)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pacientes', function (Blueprint $table) {
            //
            $table->dropColumn('endereco');
            $table->dropColumn('cep');
            $table->dropColumn('referencia');
            $table->dropColumn('profissao');
            $table->dropColumn('local_trabalho');
            $table->dropColumn('renda');
        });
    }
}
