<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 100);
            $table->string('apelido', 20);
            $table->date('data_nascimento');
            $table->integer('idade');
            $table->string('sexo');
            $table->string('nome_mae', 100);
            $table->string('nome_pai', 100);
            $table->integer('cns');
            $table->string('cpf', 11)->unique();
            $table->integer('rg');
            $table->date('data_expedicao');
            $table->string('naturalidade', 30);
            $table->string('estado_civil');
            $table->integer('filhos');
            $table->string('escolaridade');
            $table->string('etnia');
            $table->string('reside_com');
            $table->integer('telefone');

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
