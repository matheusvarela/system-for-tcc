<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriagemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('triagems', function (Blueprint $table) {
            $table->increments('id');
            $table->date('data');
            $table->string('entrevistador', 50);
            $table->boolean('espontanea')->nullable();
            $table->string('encaminhado', 30)->nullable();
            $table->string('etaria')->nullable();
            $table->string('deficiencia')->nullable();
            $table->string('cid');
            $table->string('psf')->nullable();
            $table->boolean('fora_area')->nullable();
            $table->string('motivacao_buscar', 255)->nullable();
            $table->string('ultima_vez', 20)->nullable();
            $table->string('doenca_familiar')->nullable();
            $table->boolean('parar_sem_ajuda')->nullable();
            $table->string('estrategias')->nullable();
            $table->boolean('trat_anterior')->nullable();
            $table->string('vezes')->nullable();
            $table->string('local')->nullable();
            $table->boolean('problemas_justica_policia')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('triagems');
    }
}
