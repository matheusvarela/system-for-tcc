<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableTablePacientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pacientes', function (Blueprint $table) {
            //
            $table->string('apelido', 20)->nullable()->change();
            $table->string('idade', 2)->nullable()->change();
            $table->string('nome_pai', 100)->nullable()->change();
            $table->string('naturalidade', 30)->nullable()->change();
            $table->string('estado_civil', 15)->nullable()->change();
            $table->string('filhos')->nullable()->change();
            $table->string('escolaridade')->nullable()->change();
            $table->string('etnia')->nullable()->change();
            $table->string('reside_com')->nullable()->change();
            $table->string('telefone', 11)->nullable()->change();
            $table->string('endereco', 100)->nullable()->change();
            $table->string('cep', 8)->nullable()->change();
            $table->string('referencia', 100)->nullable()->change();
            $table->string('profissao', 30)->nullable()->change();
            $table->string('local_trabalho', 30)->nullable()->change();
            $table->string('renda', 30)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pacientes', function (Blueprint $table) {
            //
            $table->dropColumn(['apelido', 'idade', 'nome_pai', 'naturalidade', 'estado_civil', 'filhos', 'escolaridade', 'etnia',
          'reside_com', 'telefone', 'endereco', 'cep', 'referencia', 'profissao', 'local_trabalho', 'renda']);
        });
    }
}
