<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNovosCamposTableEstabelecimento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estabelecimentos', function (Blueprint $table) {
            //
            $table->renameColumn('nome_fantasia', 'nome');
        });
        Schema::table('estabelecimentos', function (Blueprint $table) {
            //
            $table->string('natureza_juridica')->default('');
            $table->string('nome_empresarial')->default('');
            $table->string('cnpj')->default('')->nullable();
            $table->string('logradouro')->default('');
            $table->integer('numero')->default('')->nullable();
            $table->string('complemento')->default('')->nullable();
            $table->string('bairro')->default('');
            $table->string('uf')->default('');
            $table->string('cep')->default('');
            $table->string('telefone')->default('')->nullable();
            $table->string('dependencia')->default('');
            $table->integer('regional_saude')->default('')->nullable();
            $table->string('tipo')->default('');
            $table->string('subtipo')->default('')->nullable();
            $table->string('gestao')->default('');
        });

        Schema::table('estabelecimentos', function (Blueprint $table) {
            //
            $table->integer('administrador')->unsigned()->nullable();

            $table->foreign('administrador')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estabelecimentos', function (Blueprint $table) {
            //
            $table->dropColumn(['nome', 'natureza_juridica', 'nome_empresarial', 'cnpj', 'logradouro', 'numero', 'complemento', 'bairro', 'uf', 'cep', 'telefone', 'dependencia', 'regional_saude', 'tipo', 'subtipo', 'gestao', 'administrador']);
        });

        Schema::table('estabelecimentos', function (Blueprint $table) {
            //
            $table->dropForeign(['administrador']);
        });
    }
}
