<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNovosCamposUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('cpf', 11)->unique()->default('');
            $table->string('matricula')->unique()->default('');
            $table->string('funcao')->default('');
            $table->string('cargo')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('cpf');
            $table->dropColumn('matricula');
            $table->dropColumn('funcao');
            $table->dropColumn('cargo');
        });
    }
}
