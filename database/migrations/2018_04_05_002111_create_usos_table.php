<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('substancia_id');
            $table->integer('triagem_id');
            $table->string('tempo_uso');
            $table->string('freq_uso');
            $table->string('max_freq_uso');

            $table->foreign('substancia_id')->references('id')->on('substancias')->onDelete('cascade');
            $table->foreign('triagem_id')->references('id')->on('triagems')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::table('triagems', function (Blueprint $table) {
            //
            $table->integer('uso_id')->unsigned()->nullable();
            $table->foreign('uso_id')->references('id')->on('usos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usos');

        Schema::table('triagems', function (Blueprint $table) {
            //
            $table->dropForeign('uso_id');
        });
    }
}
