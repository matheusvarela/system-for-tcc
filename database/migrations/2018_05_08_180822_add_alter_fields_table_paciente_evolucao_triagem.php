<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlterFieldsTablePacienteEvolucaoTriagem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('evolucoes', function (Blueprint $table) {
            //
            $table->dropColumn(['rubrica', 'paciente_id']);
        });

        Schema::table('evolucoes', function (Blueprint $table) {
            //
            $table->dropForeign(['paciente_id']);
        });

        Schema::table('evolucoes', function (Blueprint $table) {
            //
            $table->integer('triagem_id')->unsigned()->nullable();
            $table->foreign('triagem_id')->references('id')->on('triagems')->onDelete('cascade');
        });

        Schema::table('evolucoes', function (Blueprint $table) {
            //
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
