<?php

use Illuminate\Database\Seeder;

class SubstanciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('substancias')->insert([
          'nome' => 'Tabaco',
        ]);

        DB::table('substancias')->insert([
          'nome' => 'Álcool',
        ]);

        DB::table('substancias')->insert([
          'nome' => 'Maconha',
        ]);

        DB::table('substancias')->insert([
          'nome' => 'Crack',
        ]);

        DB::table('substancias')->insert([
          'nome' => 'Mesclado',
        ]);

        DB::table('substancias')->insert([
          'nome' => 'Bebinho',
        ]);

        DB::table('substancias')->insert([
          'nome' => 'Cocaína',
        ]);

        DB::table('substancias')->insert([
          'nome' => 'Injetáveis',
        ]);
    }
}
