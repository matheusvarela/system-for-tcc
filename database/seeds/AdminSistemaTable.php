<?php

use Illuminate\Database\Seeder;

class AdminSistemaTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('admins')->insert([
          'name' => 'Administrador',
          'email' => 'math.varela1622@gmail.com',
          'password' => Hash::make('123456'),
        ]);
    }
}
