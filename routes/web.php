<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('pacientes', 'PacienteController');
Route::get('search',array('as'=>'search','uses'=> 'PacienteController@search'));
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::prefix('triagems')->group(function(){
  Route::get('/create/{id}', 'TriagemController@create');
  Route::post('{id}', 'TriagemController@store');
  Route::get('/list/{id}', 'TriagemController@index');
  Route::get('{id}/edit', 'TriagemController@edit');
  Route::get('{id}/retriagem', 'TriagemController@retriagem');
  Route::patch('{id}', 'TriagemController@update');
  Route::get('/show/{id}', 'TriagemController@show');
});

Route::prefix('funcionario')->name('funcionario.')->group(function(){
  Route::get('/create', 'FuncionarioController@create')->name('create');
  Route::get('{id}/edit', 'FuncionarioController@edit')->name('edit');
  Route::get('/', 'FuncionarioController@index')->name('index');
  Route::post('/', 'FuncionarioController@store');
  Route::patch('{id}', 'FuncionarioController@update');
  Route::delete('{id}', 'FuncionarioController@destroy');
  Route::get('show/{id}', 'FuncionarioController@show');
});

Route::prefix('evolucoes')->group(function(){
  Route::get('/create/{evolucao}', 'EvolucaoController@create');
  Route::post('{evolucao}', 'EvolucaoController@store');
  Route::get('/list/{id}', 'EvolucaoController@index');
  Route::get('/show/{id}', 'EvolucaoController@show');
  Route::get('{id}/edit', 'EvolucaoController@edit');
  Route::delete('{id}', 'EvolucaoController@destroy');
  Route::patch('{id}', 'EvolucaoController@update');
});

Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function () {
    Auth::routes();
    Route::middleware('auth:admin')->group(function () {
        Route::get('/', 'Auth\LoginController@showLoginForm');
        Route::resource('estabelecimentos', 'EstabelecimentoController');
        Route::resource('funcionarios', 'AdmEstabelecimentoController');
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('autocomplete',array('as'=>'autocomplete','uses'=> 'EstabelecimentoController@autocomplete'));
    });
});
