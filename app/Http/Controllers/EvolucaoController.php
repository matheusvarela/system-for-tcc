<?php

namespace App\Http\Controllers;

use App\Evolucao;
use App\Paciente;
use App\Triagem;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EvolucaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        //
        $triagems = Triagem::where('paciente_id', $id)->get();

        $evolucoes = Evolucao::with('triagems')
          ->leftJoin('triagems', function ($join) use ($id) {
              $join->on('evolucoes.triagem_id', '=', 'triagems.id')
                   ->where('triagems.paciente_id', '=', $id);
          })
          ->select('evolucoes.*')
          ->orderBy('created_at', 'asc')->distinct()->paginate(3);

          return view('evolucoes.index', compact('evolucoes', 'triagems'));
    }


    public function create($id)
    {
        //
        $triagem = Triagem::findOrFail($id);

        return view('evolucoes.create', compact('triagem'));
    }


    public function store(Request $request, $id)
    {
        //

        $this->validate($request, [
          'data' => 'required|date_format:d/m/Y',
          'historico' => 'required|string|max:255',
          'diagnostico' => 'required|string|max:255',
          'conduta' => 'required|string|max:255',
        ]);

        $evolucao = new Evolucao([
          'data' => $request->get('data'),
          'historico' => $request->get('historico'),
          'diagnostico' => $request->get('diagnostico'),
          'conduta' => $request->get('conduta'),
        ]);

        $triagem = Triagem::where('id', $id)->orderBy('created_at', 'asc')->first();

        $triagem->evolucoes()->save($evolucao);

        $user = User::where([
          ['name', Auth::user()->name],
          ['id', Auth::user()->id],
        ])->first();

        $user->evolucoes()->save($evolucao);

        return redirect()->action('EvolucaoController@index', $triagem->paciente_id);

    }

    public function show($id)
    {
        //
        $evolucao = Evolucao::findOrFail($id);

        return view('evolucoes.show', compact('evolucao'));

    }

    public function edit($id)
    {
        //
        $evolucao = Evolucao::findOrFail($id);

        return view('evolucoes.edit', compact('evolucao'));
    }

    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
          'historico' => 'required|string|max:255',
          'diagnostico' => 'required|string|max:255',
          'conduta' => 'required|string|max:255',
        ]);

        $evolucao = Evolucao::findOrFail($id);

        $evolucao->historico = $request->get('historico');
        $evolucao->diagnostico = $request->get('diagnostico');
        $evolucao->conduta = $request->get('conduta');

        $evolucao->save();

        $triagem = Triagem::findOrFail($evolucao->triagem_id);

        return redirect()->action('EvolucaoController@index', $triagem->paciente_id);
    }

    public function destroy($id)
    {
        //
        $evolucao = Evolucao::findOrFail($id);
        $triagem_id = $evolucao->triagem_id;
        $triagem = Triagem::findOrFail($triagem_id);
        $evolucao->delete();

        return redirect()->action('EvolucaoController@index', $triagem->paciente_id);
    }
}
