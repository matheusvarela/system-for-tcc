<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Paciente;
use App\Estabelecimento;
use App\Triagem;
Use App\Uso;
Use App\Evolucao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


class PacienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $search = Input::get('search');
        if ($search != "" || $search != NULL) {
          # code...
          $pacientes = Paciente::where([
            ['estabelecimento_id', Auth::user()->estabelecimento_id],
            ['nome', 'LIKE', '%'.$search.'%']
          ])->orWhere([
            ['estabelecimento_id', Auth::user()->estabelecimento_id],
            ['cpf', 'LIKE', '%'.$search.'%']
          ])->orWhere([
            ['estabelecimento_id', Auth::user()->estabelecimento_id],
            ['cns', 'LIKE', '%'.$search.'%']
          ])->distinct()->orderBy('nome', 'asc')->paginate(4);

          return view('pacientes.index', compact('pacientes'));
        } else {
          # code...
          $pacientes = Paciente::where('estabelecimento_id', Auth::user()->estabelecimento_id)->orderBy('nome', 'asc')->paginate(4);

          return view('pacientes.index', compact('pacientes'));
        }
    }


    public function create()
    {
        //
        return view('pacientes.create');
    }

    public function store(Request $request)
    {
        //

        $this->validate($request, [
          'nome' => 'required|string|max:255',
          'apelido' => 'string|max:255|nullable',
          'data_nascimento' => 'required|date_format:d/m/Y|before:data_expedicao',
          'idade' => 'string|max:2|nullable',
          'sexo' => 'string|max:9|nullable',
          'nome_mae' => 'required|string|max:255',
          'nome_pai' => 'string|max:255|nullable',
          'cns' => 'required|string',
          'cpf' => 'required|string|min:11|max:14|unique:pacientes',
          'rg' => 'required|string',
          'data_expedicao' => 'required|date_format:d/m/Y|after:data_nascimento',
          'naturalidade' => 'string|max:255|nullable',
          'etnia' => 'string|max:255|nullable',
          'estado_civil' => 'string|max:255|nullable',
          'filhos' => 'string|max:2|nullable',
          'escolaridade' => 'string|max:255|nullable',
          'telefone' => 'string|max:14|nullable',
          'endereco' => 'string|max:255|nullable',
          'cep' => 'required|string|min:8|max:9',
          'referencia' => 'string|max:255|nullable',
          'reside_com' => 'string|max:255|nullable',
          'profissao' => 'string|max:255|nullable',
          'local_trabalho' => 'string|max:255|nullable',
          'renda' => 'string|max:255|nullable',

        ]);

        $paciente = new Paciente([
          'nome' => $request->get('nome'),
          'apelido' => $request->get('apelido'),
          'data_nascimento' => $request->get('data_nascimento'),
          'idade' => $request->get('idade'),
          'sexo' => $request->get('sexo'),
          'nome_mae' => $request->get('nome_mae'),
          'nome_pai' => $request->get('nome_pai'),
          'cns' => $request->get('cns'),
          'cpf' => preg_replace('~\D~', '',$request->get('cpf')),
          'rg' => preg_replace('~\D~', '',$request->get('rg')),
          'data_expedicao' => $request->get('data_expedicao'),
          'naturalidade' => $request->get('naturalidade'),
          'estado_civil' => $request->get('estado_civil'),
          'filhos' => $request->get('filhos'),
          'escolaridade' => $request->get('escolaridade'),
          'etnia' => $request->get('etnia'),
          'reside_com' => $request->get('reside_com'),
          'telefone' => preg_replace('~\D~', '',$request->get('telefone')),
          'endereco' => $request->get('endereco'),
          'cep' => preg_replace('~\D~', '',$request->get('cep')),
          'referencia' => $request->get('referencia'),
          'profissao' => $request->get('profissao'),
          'local_trabalho' => $request->get('local_trabalho'),
          'renda' => $request->get('renda'),
        ]);

        $estabelecimento = Estabelecimento::where([
          ['nome', Auth::user()->estabelecimentos->nome],
          ['id', Auth::user()->estabelecimentos->id],
        ])->first();

        $estabelecimento->pacientes()->save($paciente);

        return redirect()->route('pacientes.index');
    }


    public function show($id)
    {
        //
        $paciente = Paciente::findOrFail($id);

        return view('pacientes.show', compact('paciente'));
    }


    public function edit($id)
    {
        //
        $paciente = Paciente::findOrFail($id);

        return view('pacientes.edit', compact('paciente', 'id'));
    }


    public function update(Request $request, $id)
    {
        //
        $paciente = Paciente::findOrFail($id);

        $this->validate($request, [
          'nome' => 'required|string|max:255',
          'apelido' => 'string|max:255|nullable',
          'data_nascimento' => 'required|date_format:d/m/Y|before:data_expedicao',
          'idade' => 'string|max:2|nullable',
          'sexo' => 'string|max:9|nullable',
          'nome_mae' => 'required|string|max:255',
          'nome_pai' => 'string|max:255|nullable',
          'cns' => 'required|string',
          'cpf' => 'required|string|min:11|max:14|unique:pacientes,'.$paciente->id,
          'rg' => 'required|string',
          'data_expedicao' => 'required|date_format:d/m/Y|after:data_nascimento',
          'naturalidade' => 'string|max:255|nullable',
          'etnia' => 'string|max:255|nullable',
          'estado_civil' => 'string|max:255|nullable',
          'filhos' => 'string|max:2|nullable',
          'escolaridade' => 'string|max:255|nullable',
          'telefone' => 'string|max:11|nullable',
          'endereco' => 'string|max:255|nullable',
          'cep' => 'required|string|min:8|max:9',
          'referencia' => 'string|max:255|nullable',
          'reside_com' => 'string|max:255|nullable',
          'profissao' => 'string|max:255|nullable',
          'local_trabalho' => 'string|max:255|nullable',
          'renda' => 'string|max:255|nullable',

        ]);

        $paciente->nome = $request->get('nome');
        $paciente->apelido = $request->get('apelido');
        $paciente->data_nascimento = preg_replace('~\D~', '',$request->get('data_nascimento'));
        $paciente->idade = $request->get('idade');
        $paciente->sexo = $request->get('sexo');
        $paciente->nome_mae = $request->get('nome_mae');
        $paciente->nome_pai = $request->get('nome_pai');
        $paciente->cns = $request->get('cns');
        $paciente->cpf = preg_replace('~\D~', '', $request->get('cpf'));
        $paciente->rg = preg_replace('~\D~', '',$request->get('rg'));
        $paciente->data_expedicao = preg_replace('~\D~', '',$request->get('data_expedicao'));
        $paciente->naturalidade = $request->get('naturalidade');
        $paciente->estado_civil = $request->get('estado_civil');
        $paciente->filhos = $request->get('filhos');
        $paciente->escolaridade = $request->get('escolaridade');
        $paciente->etnia = $request->get('etnia');
        $paciente->reside_com = $request->get('reside_com');
        $paciente->telefone = preg_replace('~\D~', '',$request->get('telefone'));
        $paciente->endereco = $request->get('endereco');
        $paciente->cep = preg_replace('~\D~', '',$request->get('cep'));
        $paciente->referencia = $request->get('referencia');
        $paciente->profissao = $request->get('profissao');
        $paciente->local_trabalho = $request->get('local_trabalho');
        $paciente->renda = $request->get('renda');

        $paciente->save();

        return redirect()->route('pacientes.index');


    }


    public function destroy($id)
    {
        //
        $paciente = Paciente::findOrFail($id);

        $triagems = Triagem::where('paciente_id', $paciente->id)->get();

        foreach ($triagems as $triagem) {
          # code...
          foreach ($triagem->evolucoes as $evolucao) {
            # code...
            $evolucao->delete();
          }

          foreach ($triagem->usos as $uso) {
            # code...
            $uso->delete();
          }

          $triagem->delete();

        }

        $paciente->delete();

        return redirect()->route('pacientes.index');

    }
}
