<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Estabelecimento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


class FuncionarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        $search = Input::get('search_func');
        if ($search != "" || $search != NULL) {
          # code...

          $funcionarios = User::where([
            ['estabelecimento_id', Auth::user()->estabelecimento_id],
            ['name', 'LIKE', '%'.$search.'%']
          ])->orWhere([
            ['estabelecimento_id', Auth::user()->estabelecimento_id],
            ['cpf', 'LIKE', '%'.$search.'%']
          ])->orWhere([
            ['estabelecimento_id', Auth::user()->estabelecimento_id],
            ['email', 'LIKE', '%'.$search.'%']
          ])->orWhere([
            ['estabelecimento_id', Auth::user()->estabelecimento_id],
            ['matricula', 'LIKE', '%'.$search.'%']
          ])->distinct()->orderBy('name', 'asc')->paginate(3);

          return view('funcionario.index', compact('funcionarios'));
        } else {
          $funcionarios = User::where([
            ['estabelecimento_id', Auth::user()->estabelecimento_id]
            ])->distinct()->orderBy('name', 'asc')->paginate(3);

            return view('funcionario.index', compact('funcionarios'));
        }
    }


    public function create()
    {
        //
        return view('funcionario.create');
    }


    public function store(Request $request)
    {
        //

        $this->validate($request, [
          'name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255|unique:users',
          'password' => 'required|string|min:6|confirmed',
          'cpf' => 'required|string|min:11|max:14|unique:users',
          'matricula' => 'required|string|unique:users',
          'funcao' => 'required|string|max:255',
          'cargo' => 'required|string|max:255',
          'estabelecimento' => 'required|string|max:255',
        ]);

        $funcionario = new User([
          'name' => $request->get('name'),
          'email' => $request->get('email'),
          'password' => bcrypt($request->get('password')),
          'cpf' => $request->get('cpf'),
          'matricula' => $request->get('matricula'),
          'funcao' => $request->get('funcao'),
          'cargo' => $request->get('cargo'),
        ]);

        $estabelecimento = Estabelecimento::where('nome', $request->get('estabelecimento'))->first();

        $estabelecimento->users()->save($funcionario);

        return redirect()->route('funcionario.index');
    }


    public function show($id)
    {
        //
        $funcionario = User::findOrFail($id);

        return view('funcionario.show', compact('funcionario'));
    }


    public function edit($id)
    {
        //
        $funcionario = User::where('id', $id)->first();
        return view('funcionario.edit', compact('funcionario'));
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255|unique:users,'.$id,
          'password' => 'required|string|min:6|confirmed',
          'cpf' => 'required|string|min:11|max:14|unique:users,'.$id,
          'matricula' => 'required|string|unique:users,'.$id,
          'funcao' => 'required|string|max:255',
          'cargo' => 'required|string|max:255',
        ]);

        $funcionario = User::where('id', $id)->first();

        $funcionario->name = $request->get('name');
        $funcionario->email = $request->get('email');
        $funcionario->password = bcrypt($request->get('password'));
        $funcionario->cpf = $request->get('cpf');
        $funcionario->matricula = $request->get('matricula');
        $funcionario->funcao = $request->get('funcao');
        $funcionario->cargo = $request->get('cargo');

        $funcionario->save();

        return redirect()->route('funcionario.index');

    }


    public function destroy($id)
    {
        //
        $funcionario = User::findOrFail($id);

        $funcionario->delete();

        return redirect()->route('funcionario.index');

    }
}
