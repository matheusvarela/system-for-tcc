<?php

namespace App\Http\Controllers;

use App\Triagem;
use App\Paciente;
use App\Substancia;
use App\Uso;
use App\User;
use App\Estabelecimento;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class TriagemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($id)
    {
        //
        $triagems = Triagem::where('paciente_id', $id)->orderBy('created_at', 'asc')->paginate(5);
        return view('triagens.index', compact('id', 'triagems'));
    }


    public function create($id)
    {
        //
        $paciente = Paciente::findOrFail($id);
        return view('triagens.create', compact('paciente'));
    }


    public function store(Request $request, $id)
    {
        //

        $this->validate($request, [
          'data' => 'required|date_format:d/m/Y',
          'espontanea' => 'nullable|boolean',
          'encaminhado' => 'nullable|string|max:255',
          'etaria' => 'nullable|string|max:40',
          'deficiencia' => 'nullable|string|max:100',
          'cid' => 'required|string|min:3|max:4',
          'psf' => 'nullable|string|max:255',
          'fora_area' => 'nullable|boolean',
          'motivacao_buscar' => 'nullable|string:max:255',
          'ultima_vez' => 'nullable|string|max:50',
          'doenca_familiar' => 'nullable|string|max:100',
          'parar_sem_ajuda' => 'nullable|boolean',
          'estrategias' => 'nullable|string|max:255',
          'trat_anterior' => 'nullable|boolean',
          'vezes' => 'nullable|string|max:3',
          'local' => 'nullable|string|max:255',
          'problemas_justica_policia' => 'nullable|boolean',

        ]);

        $triagem = new Triagem([
          'data' => $request->get('data'),
          'espontanea' => $request->get('espontanea'),
          'encaminhado' => $request->get('encaminhado'),
          'etaria' => $request->get('etaria'),
          'deficiencia' => $request->get('deficiencia'),
          'cid' => $request->get('cid'),
          'psf' => $request->get('psf'),
          'fora_area' => $request->get('fora_area'),
          'motivacao_buscar' => $request->get('motivacao_buscar'),
          'ultima_vez' => $request->get('ultima_vez'),
          'doenca_familiar' => $request->get('doenca_familiar'),
          'parar_sem_ajuda' => $request->get('parar_sem_ajuda'),
          'estrategias' => $request->get('estrategias'),
          'trat_anterior' => $request->get('trat_anterior'),
          'vezes' => $request->get('vezes'),
          'local' => $request->get('local'),
          'problemas_justica_policia' => $request->get('problemas_justica_policia'),
        ]);

        $estabelecimento = Estabelecimento::where([
          ['nome', Auth::user()->estabelecimentos->nome],
          ['id', Auth::user()->estabelecimentos->id],
        ])->first();

        $estabelecimento->triagems()->save($triagem);

        $paciente = Paciente::where([
          ['nome', $request->get('nome')],
          ['id', $id],
        ])->first();


        $paciente->triagems()->save($triagem);

        $user = User::where([
          ['name', Auth::user()->name],
          ['id', Auth::user()->id],
        ])->first();

        $user->triagems()->save($triagem);


        if ($request->get('tab_tempo_uso') && $request->get('tab_freq_uso') && $request->get('tab_max_freq_uso')) {
          # code...
          $uso_tab = new Uso([
            'substancia_id' => 1,
            'tempo_uso' => $request->get('tab_tempo_uso'),
            'freq_uso' => $request->get('tab_freq_uso'),
            'max_freq_uso' => $request->get('tab_max_freq_uso'),
          ]);

          $triagem->usos()->save($uso_tab);
        }

        if ($request->get('alc_tempo_uso') && $request->get('alc_freq_uso') && $request->get('alc_max_freq_uso')) {
          # code...
          $uso_alc = new Uso([
            'substancia_id' => 2,
            'tempo_uso' => $request->get('alc_tempo_uso'),
            'freq_uso' => $request->get('alc_freq_uso'),
            'max_freq_uso' => $request->get('alc_max_freq_uso'),
          ]);

          $triagem->usos()->save($uso_alc);
        }

        if ($request->get('mac_tempo_uso') && $request->get('mac_freq_uso') && $request->get('mac_max_freq_uso')) {
          # code...
          $uso_mac = new Uso([
            'substancia_id' => 3,
            'tempo_uso' => $request->get('mac_tempo_uso'),
            'freq_uso' => $request->get('mac_freq_uso'),
            'max_freq_uso' => $request->get('mac_max_freq_uso'),
          ]);

          $triagem->usos()->save($uso_mac);
        }

        if ($request->get('cra_tempo_uso') && $request->get('cra_freq_uso') && $request->get('cra_max_freq_uso')) {
          # code...
          $uso_cra = new Uso([
            'substancia_id' => 4,
            'tempo_uso' => $request->get('cra_tempo_uso'),
            'freq_uso' => $request->get('cra_freq_uso'),
            'max_freq_uso' => $request->get('cra_max_freq_uso'),
          ]);

          $triagem->usos()->save($uso_cra);
        }

        if ($request->get('mes_tempo_uso') && $request->get('mes_freq_uso') && $request->get('mes_max_freq_uso')) {
          # code...
          $uso_mes = new Uso([
            'substancia_id' => 5,
            'tempo_uso' => $request->get('mes_tempo_uso'),
            'freq_uso' => $request->get('mes_freq_uso'),
            'max_freq_uso' => $request->get('mes_max_freq_uso'),
          ]);

          $triagem->usos()->save($uso_mes);
        }

        if ($request->get('beb_tempo_uso') && $request->get('beb_freq_uso') && $request->get('beb_max_freq_uso')) {
          # code...
          $uso_beb = new Uso([
            'substancia_id' => 6,
            'tempo_uso' => $request->get('beb_tempo_uso'),
            'freq_uso' => $request->get('beb_freq_uso'),
            'max_freq_uso' => $request->get('beb_max_freq_uso'),
          ]);

          $triagem->usos()->save($uso_beb);
        }

        if ($request->get('coc_tempo_uso') && $request->get('coc_freq_uso') && $request->get('coc_max_freq_uso')) {
          # code...
          $uso_coc = new Uso([
            'substancia_id' => 7,
            'tempo_uso' => $request->get('coc_tempo_uso'),
            'freq_uso' => $request->get('coc_freq_uso'),
            'max_freq_uso' => $request->get('coc_max_freq_uso'),
          ]);

          $triagem->usos()->save($uso_coc);
        }

        if ($request->get('inj_tempo_uso') && $request->get('inj_freq_uso') && $request->get('inj_max_freq_uso')) {
          # code...
          $uso_inj = new Uso([
            'substancia_id' => 8,
            'tempo_uso' => $request->get('inj_tempo_uso'),
            'freq_uso' => $request->get('inj_freq_uso'),
            'max_freq_uso' => $request->get('inj_max_freq_uso'),
          ]);

          $triagem->usos()->save($uso_inj);
        }

        if ($request->get('subst_outras')) {
          # code...

          $newSubstancia = new Substancia([
            'nome' => $request->get('subst_outras'),
          ]);

          $newSubstancia->save();

          $substancia_id = Substancia::where('nome', $request->get('subst_outras'))->value('id');
          $newUso = new Uso([
            'substancia_id' => $substancia_id,
            'tempo_uso' => $request->get('out_tempo_uso'),
            'freq_uso' => $request->get('out_freq_uso'),
            'max_freq_uso' => $request->get('out_max_freq_uso'),
          ]);

          $triagem->usos()->save($newUso);

        }

        return redirect()->action('TriagemController@index', $id);
    }


    public function show($id)
    {
        //
        $triagem = Triagem::where('id', $id)->first();


        $usos = DB::table('usos')
                            ->join('substancias', function($join) use ($id) {
                                $join->on('usos.substancia_id', '=', 'substancias.id')
                                  ->where('triagem_id', '=', $id);
                          })->get();
        $list_not_subs = DB::table('substancias')
                                      ->whereNotExists(function($query)
                                      {
                                        $query->select(DB::raw(1))
                                              ->from('usos')
                                              ->whereRaw('usos.substancia_id = substancias.id');
                                      })->get();
        return view('triagens.show', compact('triagem', 'usos', 'list_not_subs'));
    }


    public function edit($id)
    {
        //
        $triagem = Triagem::findOrFail($id);
        $paciente = Paciente::where('id', $triagem->paciente_id)->value('nome');
        $usos = DB::table('usos')
                            ->join('substancias', function($join) use ($id) {
                                $join->on('usos.substancia_id', '=', 'substancias.id')
                                  ->where('triagem_id', '=', $id);
                          })->get();
        $list_not_subs = DB::table('substancias')
                                      ->whereNotExists(function($query)
                                      {
                                        $query->select(DB::raw(1))
                                              ->from('usos')
                                              ->whereRaw('usos.substancia_id = substancias.id');
                                      })->get();

        return view('triagens.edit', compact('triagem', 'id', 'paciente', 'list_not_subs', 'usos'));
    }

    public function retriagem($id)
    {
        //
        $triagem = Triagem::findOrFail($id);
        $paciente = Paciente::where('id', $triagem->paciente_id)->value('nome');
        $usos = DB::table('usos')
                            ->join('substancias', function($join) use ($id) {
                                $join->on('usos.substancia_id', '=', 'substancias.id')
                                  ->where('triagem_id', '=', $id);
                          })->get();
        $list_not_subs = DB::table('substancias')
                                      ->whereNotExists(function($query)
                                      {
                                        $query->select(DB::raw(1))
                                              ->from('usos')
                                              ->whereRaw('usos.substancia_id = substancias.id');
                                      })->get();

        return view('triagens.retriagem', compact('triagem', 'id', 'paciente', 'list_not_subs', 'usos'));
    }


    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
          'data' => 'required|date_format:d/m/Y',
          'espontanea' => 'nullable|boolean',
          'encaminhado' => 'nullable|string|max:255',
          'etaria' => 'nullable|string|max:40',
          'deficiencia' => 'nullable|string|max:100',
          'cid' => 'required|string|min:3|max:4',
          'psf' => 'nullable|string|max:255',
          'fora_area' => 'nullable|boolean',
          'motivacao_buscar' => 'nullable|string:max:255',
          'ultima_vez' => 'nullable|string|max:50',
          'doenca_familiar' => 'nullable|string|max:100',
          'parar_sem_ajuda' => 'nullable|boolean',
          'estrategias' => 'nullable|string|max:255',
          'trat_anterior' => 'nullable|boolean',
          'vezes' => 'nullable|string|max:3',
          'local' => 'nullable|string|max:255',
          'problemas_justica_policia' => 'nullable|boolean',

        ]);

        $triagem = Triagem::findOrFail($id);


        $triagem->espontanea = $request->get('espontanea');
        $triagem->encaminhado = $request->get('encaminhado');
        $triagem->etaria = $request->get('etaria');
        $triagem->deficiencia = $request->get('deficiencia');
        $triagem->psf = $request->get('psf');
        $triagem->cid = $request->get('cid');
        $triagem->fora_area = $request->get('fora_area');
        $triagem->motivacao_buscar = $request->get('motivacao_buscar');
        $triagem->ultima_vez = $request->get('ultima_vez');
        $triagem->doenca_familiar = $request->get('doenca_familiar');
        $triagem->parar_sem_ajuda = $request->get('parar_sem_ajuda');
        $triagem->estrategias = $request->get('estrategias');
        $triagem->trat_anterior = $request->get('trat_anterior');
        $triagem->vezes = $request->get('vezes');
        $triagem->local = $request->get('local');
        $triagem->problemas_justica_policia = $request->get('problemas_justica_policia');

        $triagem->save();


        // TABACO

        if ($request->get('tab_tempo_uso') && $request->get('tab_freq_uso') && $request->get('tab_max_freq_uso')) {
          # code...

          $uso_tab = Uso::where('triagem_id', $id)->where('substancia_id', 1)->first();
          //echo gettype($uso_tab);
          if ($uso_tab == NULL) {
            # code...
            $newUsoTab = new Uso([
              'substancia_id' => 1,
              'tempo_uso' => $request->get('tab_tempo_uso'),
              'freq_uso' => $request->get('tab_freq_uso'),
              'max_freq_uso' => $request->get('tab_max_freq_uso'),
            ]);

            $triagem->usos()->save($newUsoTab);

          } else {
            # code...
            $uso_tab->tempo_uso = $request->get('tab_tempo_uso');
            $uso_tab->freq_uso = $request->get('tab_freq_uso');
            $uso_tab->max_freq_uso = $request->get('tab_max_freq_uso');

            $uso_tab->save();
          }
        }

        // ALCOOL

        if ($request->get('alc_tempo_uso') && $request->get('alc_freq_uso') && $request->get('alc_max_freq_uso')) {
          # code...

          $uso_alc = Uso::where('triagem_id', $id)->where('substancia_id', 2)->first();

          if ($uso_alc == NULL) {
            # code...
            $newUsoAlc = new Uso([
              'substancia_id' => 2,
              'tempo_uso' => $request->get('alc_tempo_uso'),
              'freq_uso' => $request->get('alc_freq_uso'),
              'max_freq_uso' => $request->get('alc_max_freq_uso'),
            ]);

            $triagem->usos()->save($newUsoAlc);

          } else {
            # code...
            $uso_alc->tempo_uso = $request->get('alc_tempo_uso');
            $uso_alc->freq_uso = $request->get('alc_freq_uso');
            $uso_alc->max_freq_uso = $request->get('alc_max_freq_uso');

            $uso_alc->save();
          }

        }

        // MACONHA

        if ($request->get('mac_tempo_uso') && $request->get('mac_freq_uso') && $request->get('mac_max_freq_uso')) {
          # code...
          $uso_mac = Uso::where('triagem_id', $id)->where('substancia_id', 3)->first();

          if ($uso_mac == NULL) {
            # code...
            $newUsoMac = new Uso([
              'substancia_id' => 3,
              'tempo_uso' => $request->get('mac_tempo_uso'),
              'freq_uso' => $request->get('mac_freq_uso'),
              'max_freq_uso' => $request->get('mac_max_freq_uso'),
            ]);

            $triagem->usos()->save($newUsoMac);

          }else {
            # code...
            $uso_mac->tempo_uso = $request->get('mac_tempo_uso');
            $uso_mac->freq_uso = $request->get('mac_freq_uso');
            $uso_mac->max_freq_uso = $request->get('mac_max_freq_uso');

            $uso_mac->save();
          }
        }

        // CRACK

        if ($request->get('cra_tempo_uso') && $request->get('cra_freq_uso') && $request->get('cra_max_freq_uso')) {
          # code...
          $uso_cra = Uso::where('triagem_id', $id)->where('substancia_id', 4)->first();

          if ($uso_cra == NULL) {
            # code...
            $newUsoCra = new Uso([
              'substancia_id' => 1,
              'tempo_uso' => $request->get('cra_tempo_uso'),
              'freq_uso' => $request->get('cra_freq_uso'),
              'max_freq_uso' => $request->get('cra_max_freq_uso'),
            ]);

            $triagem->usos()->save($newUsoCra);

          }else {
            # code...
            $uso_cra->tempo_uso = $request->get('cra_tempo_uso');
            $uso_cra->freq_uso = $request->get('cra_freq_uso');
            $uso_cra->max_freq_uso = $request->get('cra_max_freq_uso');

            $uso_cra->save();
          }
        }

        // MESCLADO

        if ($request->get('mes_tempo_uso') && $request->get('mes_freq_uso') && $request->get('mes_max_freq_uso')) {
          # code...
          $uso_mes = Uso::where('triagem_id', $id)->where('substancia_id', 5)->first();

          if ($uso_mes == NULL) {
            # code...
            $newUsoMes = new Uso([
              'substancia_id' => 5,
              'tempo_uso' => $request->get('mes_tempo_uso'),
              'freq_uso' => $request->get('mes_freq_uso'),
              'max_freq_uso' => $request->get('mes_max_freq_uso'),
            ]);

            $triagem->usos()->save($newUsoMes);

          }else {
            # code...
            $uso_mes->tempo_uso = $request->get('mes_tempo_uso');
            $uso_mes->freq_uso = $request->get('mes_freq_uso');
            $uso_mes->max_freq_uso = $request->get('mes_max_freq_uso');

            $uso_mes->save();
          }
        }

        // BEBINHO

        if ($request->get('beb_tempo_uso') && $request->get('beb_freq_uso') && $request->get('beb_max_freq_uso')) {
          # code...
          $uso_beb = Uso::where('triagem_id', $id)->where('substancia_id', 6)->first();

          if ($uso_beb == NULL) {
            # code...
            $newUsoBeb = new Uso([
              'substancia_id' => 6,
              'tempo_uso' => $request->get('beb_tempo_uso'),
              'freq_uso' => $request->get('beb_freq_uso'),
              'max_freq_uso' => $request->get('beb_max_freq_uso'),
            ]);

            $triagem->usos()->save($newUsoBeb);

          }else {
            # code...
            $uso_beb->tempo_uso = $request->get('beb_tempo_uso');
            $uso_beb->freq_uso = $request->get('beb_freq_uso');
            $uso_beb->max_freq_uso = $request->get('beb_max_freq_uso');

            $uso_beb->save();
          }
        }

        // COCAÍNA

        if ($request->get('coc_tempo_uso') && $request->get('coc_freq_uso') && $request->get('coc_max_freq_uso')) {
          # code...
          $uso_coc = Uso::where('triagem_id', $id)->where('substancia_id', 7)->first();

          if ($uso_coc == NULL) {
            # code...
            $newUsoCoc = new Uso([
              'substancia_id' => 7,
              'tempo_uso' => $request->get('coc_tempo_uso'),
              'freq_uso' => $request->get('coc_freq_uso'),
              'max_freq_uso' => $request->get('coc_max_freq_uso'),
            ]);

            $triagem->usos()->save($newUsoCoc);

          }else {
            # code...
            $uso_coc->tempo_uso = $request->get('coc_tempo_uso');
            $uso_coc->freq_uso = $request->get('coc_freq_uso');
            $uso_coc->max_freq_uso = $request->get('coc_max_freq_uso');

            $uso_coc->save();
          }
        }

        // INJETÁVEIS

        if ($request->get('inj_tempo_uso') && $request->get('inj_freq_uso') && $request->get('inj_max_freq_uso')) {
          # code...
          $uso_inj = Uso::where('triagem_id', $id)->where('substancia_id', 8)->first();

          if ($uso_inj == NULL) {
            # code...
            $newUsoInj = new Uso([
              'substancia_id' => 8,
              'tempo_uso' => $request->get('inj_tempo_uso'),
              'freq_uso' => $request->get('inj_freq_uso'),
              'max_freq_uso' => $request->get('inj_max_freq_uso'),
            ]);

            $triagem->usos()->save($newUsoInj);

          }else {
            # code...
            $uso_inj->tempo_uso = $request->get('inj_tempo_uso');
            $uso_inj->freq_uso = $request->get('inj_freq_uso');
            $uso_inj->max_freq_uso = $request->get('inj_max_freq_uso');

            $uso_inj->save();
          }
        }

        // SUBSTANCIA CADASTRADA PELO USUARIO

        if ($request->get('out_tempo_uso') && $request->get('out_freq_uso') && $request->get('out_max_freq_uso') && $request->get('outras')) {
          # code...
          $sub_id = Substancia::where('nome', $request->get('outras'))->value('id');
          $uso_sub = Uso::where('triagem_id', $id)->where('substancia_id', $sub_id)->first();

          if ($uso_sub == NULL) {
            # code...
            $newUsoSub = new Uso([
              'substancia_id' => $sub_id,
              'tempo_uso' => $request->get('out_tempo_uso'),
              'freq_uso' => $request->get('out_freq_uso'),
              'max_freq_uso' => $request->get('out_max_freq_uso'),
              'triagem_id' => $id,
            ]);

            $triagem->usos()->save($newUsoSub);

          } else {
            # code...
            $uso_sub->tempo_uso = $request->get('out_tempo_uso');
            $uso_sub->freq_uso = $request->get('out_freq_uso');
            $uso_sub->max_freq_uso = $request->get('out_max_freq_uso');

            $uso_sub->save();
          }
        }

        // NOVA SUBSTANCIA

        if ($request->get('out_nova')) {
          # code...

          $newSubstancia = new Substancia([
            'nome' => $request->get('out_nova'),
          ]);

          $newSubstancia->save();

          $substancia_id = Substancia::where('nome', $request->get('out_nova'))->value('id');
          $newUso = new Uso([
            'substancia_id' => $substancia_id,
            'tempo_uso' => $request->get('out_nova_tempo_uso'),
            'freq_uso' => $request->get('out_nova_freq_uso'),
            'max_freq_uso' => $request->get('out_nova_max_freq_uso'),
          ]);

          $triagem->usos()->save($newUso);

        }

        return redirect()->action('TriagemController@index', $triagem->paciente_id);

    }


    public function destroy(Triagem $triagem)
    {
        //
    }
}
