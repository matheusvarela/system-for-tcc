<?php
namespace App\Http\Controllers\Admin;
use App\User;
use App\Estabelecimento;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class AdmEstabelecimentoController extends Controller
{

  function __construct()
  {
    $this->middleware('auth:admin');
  }

  protected function guard()
  {
    return Auth::guard('admin');
  }

  public function create()
  {
    return view('admin.funcionarios.create');
  }

  public function index()
  {
    $search_adm = Input::get('search_adm');

    if ($search_adm != "" || $search_adm != NULL) {
      # code...
      $funcionarios = User::where([
        ['admin', '1'],
        ['name', 'LIKE', '%'.$search_adm.'%']
      ])->orWhere([
        ['admin', '1'],
        ['cpf', 'LIKE', '%'.$search_adm.'%']
      ])->orWhere([
        ['admin', '1'],
        ['email', 'LIKE', '%'.$search_adm.'%']
      ])->orWhere([
        ['admin', '1'],
        ['matricula', 'LIKE', '%'.$search_adm.'%']
      ])->orWhere([
        ['admin', '1'],
        ['funcao', 'LIKE', '%'.$search_adm.'%']
      ])->orWhere([
        ['admin', '1'],
        ['cargo', 'LIKE', '%'.$search_adm.'%']
      ])
      ->distinct()
      ->orderBy('name', 'asc')
      ->paginate(3);

      return view('admin.funcionarios.index', compact('funcionarios'));

    } else {

      $funcionarios = User::where('admin', '1')->orderBy('name', 'asc')->paginate(3);

      return view('admin.funcionarios.index', compact('funcionarios'));
    }

  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
      'cpf' => 'required|string|min:11|max:14|unique:users',
      'matricula' => 'required|string|unique:users',
      'funcao' => 'required|string|max:255',
      'cargo' => 'required|string|max:255',
      'admin' => 'required|boolean',
      'estabelecimento' => 'required|string|max:255',
    ]);

    $estabelecimento = Estabelecimento::where('nome', $request->get('estabelecimento'))->first();


    $funcionario = new User([
      'name' => $request->get('name'),
      'email' => $request->get('email'),
      'password' => bcrypt($request->get('password')),
      'cpf' => $request->get('cpf'),
      'matricula' => $request->get('matricula'),
      'funcao' => $request->get('funcao'),
      'cargo' => $request->get('cargo'),
      'admin' => $request->get('admin'),
    ]);

    $estabelecimento->users()->save($funcionario);

    $administrador_estab = User::where([
      ['name', '=', $request->get('name')],
      ['email', '=', $request->get('email')],
      ['cpf', '=', $request->get('cpf')]])->value('id');

    $estabelecimento->administrador = $administrador_estab;
    $estabelecimento->save();

    return redirect()->route('admin.funcionarios.index');
  }

  public function show($id)
  {
      //
      $funcionario = User::findOrFail($id);

      return view('admin.funcionarios.show', compact('funcionario'));
  }

  public function edit($id)
  {
      //
      $funcionario = User::where('id', $id)->first();
      return view('admin.funcionarios.edit', compact('funcionario'));
  }

  public function update(Request $request, $id)
  {
      //
      $this->validate($request, [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users,'.$id,
        'password' => 'required|string|min:6|confirmed',
        'cpf' => 'required|string|min:11|max:14|unique:users,'.$id,
        'matricula' => 'required|string|unique:users,'.$id,
        'funcao' => 'required|string|max:255',
        'cargo' => 'required|string|max:255',
      ]);

      $funcionario = User::where('id', $id)->first();

      $funcionario->name = $request->get('name');
      $funcionario->email = $request->get('email');
      $funcionario->password = bcrypt($request->get('password'));
      $funcionario->cpf = $request->get('cpf');
      $funcionario->matricula = $request->get('matricula');
      $funcionario->funcao = $request->get('funcao');
      $funcionario->cargo = $request->get('cargo');

      $funcionario->save();

      return redirect()->route('admin.funcionarios.index');

  }


  public function destroy($id)
  {
      //
      $funcionario = User::findOrFail($id);

      $estabelecimento = $estabelecimento::where('id', $funcionario->estabelecimento_id)->update(['administrador' => NULL]);

      $funcionario->delete();

      return redirect()->route('admin.funcionarios.index');

  }


}


?>
