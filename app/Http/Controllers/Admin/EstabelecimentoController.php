<?php

namespace App\Http\Controllers\Admin;
use App\Estabelecimento;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class EstabelecimentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        //
        $search_estab = Input::get('search_estab');

        if ($search_estab != "" || $search_estab != NULL) {
          # code...
          $estabelecimentos = Estabelecimento::where([
            ['nome', 'LIKE', '%'.$search_estab.'%']
          ])->orWhere([
            ['cnes', 'LIKE', '%'.$search_estab.'%']
          ])->orWhere([
            ['cnpj', 'LIKE', '%'.$search_estab.'%']
          ])->orWhere([
            ['gestao', 'LIKE', '%'.$search_estab.'%']
          ])->orWhere([
            ['natureza_juridica', 'LIKE', '%'.$search_estab.'%']
          ])->orWhere([
            ['nome_empresarial', 'LIKE', '%'.$search_estab.'%']
          ])
          ->distinct()
          ->orderBy('nome', 'asc')
          ->paginate(3);

          return view('admin.estabelecimentos.index', compact('estabelecimentos'));

        } else {

            $estabelecimentos = Estabelecimento::orderBy('nome', 'asc')->paginate(3);

            return view('admin.estabelecimentos.index', compact('estabelecimentos'));
        }
    }

    public function autocomplete(Request $request)
    {
       //

       $estabelecimento = $request->term;
       $data = Estabelecimento::where([
         ['nome', 'LIKE', '%'.$estabelecimento.'%'],
         ['administrador', '=', NULL]])->distinct()->get();
       $result = array();

       foreach ($data as $value) {
         # code...
         $result[] = ['id' => $value->id, 'value' => $value->nome];
       }

       return response()->json($result);
    }

    public function create()
    {
        //
        return view('admin.estabelecimentos.create');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'cnes' => 'required|string|unique:estabelecimentos',
        ]);
    }

    public function store(Request $request)
    {
        //

        $this->validate($request, [
          'cnes' => 'required|string|unique:estabelecimentos',
          'nome' => 'required|string|max:255|unique:estabelecimentos',
          'cnpj' => 'nullable|string',
          'nome_empresarial' => 'required|string|max:255',
          'natureza_juridica' => 'required|string|max:255',
          'logradouro' => 'required|string|max:255',
          'numero' => 'nullable|string|max:10',
          'complemento' => 'nullable|string|max:255',
          'bairro' => 'required|string|max:255',
          'municipio' => 'required|string|max:255',
          'uf' => 'required|string|max:2',
          'cep' => 'required|string|min:8|max:9',
          'telefone' => 'nullable|string',
          'dependencia' => 'required|string|max:100',
          'regional_saude' => 'nullable|string',
          'tipo' => 'required|string|max:255',
          'subtipo' => 'nullable|string|max:255',
          'gestao' => 'required|string|max:100',
        ]);

        $estabelecimento = new Estabelecimento([
          'cnes' => $request->get('cnes'),
          'nome' => $request->get('nome'),
          'municipio' => $request->get('municipio'),
          'natureza_juridica' => $request->get('natureza_juridica'),
          'nome_empresarial' => $request->get('nome_empresarial'),
          'cnpj' => $request->get('cnpj'),
          'logradouro' => $request->get('logradouro'),
          'numero' => $request->get('numero'),
          'complemento' => $request->get('complemento'),
          'bairro' => $request->get('bairro'),
          'uf' => $request->get('uf'),
          'cep' => $request->get('cep'),
          'telefone' => $request->get('telefone'),
          'dependencia' => $request->get('dependencia'),
          'regional_saude' => $request->get('regional_saude'),
          'tipo' => $request->get('tipo'),
          'subtipo' => $request->get('subtipo'),
          'gestao' => $request->get('gestao'),
          'administrador' => $request->get('administrador'),
        ]);

        $estabelecimento->save();

        return redirect()->route('admin.estabelecimentos.index');
    }


    public function show($id)
    {
        //
        $estabelecimento = Estabelecimento::findOrFail($id);

        return view('admin.estabelecimentos.show', compact('estabelecimento'));
    }


    public function edit($id)
    {
        //
        $estabelecimento = Estabelecimento::findOrFail($id);

        return view('admin.estabelecimentos.edit', compact('estabelecimento'));
    }


    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [
          'cnes' => 'required|string|unique:estabelecimentos,'.$id,
          'nome' => 'required|string|max:255|unique:estabelecimentos,'.$id,
          'cnpj' => 'nullable|string',
          'nome_empresarial' => 'required|string|max:255',
          'natureza_juridica' => 'required|string|max:255',
          'logradouro' => 'required|string|max:255',
          'numero' => 'nullable|string|max:10',
          'complemento' => 'nullable|string|max:255',
          'bairro' => 'required|string|max:255',
          'municipio' => 'required|string|max:255',
          'uf' => 'required|string|max:2',
          'cep' => 'required|string|min:8|max:9',
          'telefone' => 'nullable|string',
          'dependencia' => 'required|string|max:100',
          'regional_saude' => 'nullable|string',
          'tipo' => 'required|string|max:255',
          'subtipo' => 'nullable|string|max:255',
          'gestao' => 'required|string|max:100',
        ]);

        $estabelecimento = Estabelecimento::findOrFail($id);

        $estabelecimento->cnes = $request->get('cnes');
        $estabelecimento->nome = $request->get('nome');
        $estabelecimento->municipio = $request->get('municipio');
        $estabelecimento->natureza_juridica = $request->get('natureza_juridica');
        $estabelecimento->nome_empresarial = $request->get('nome_empresarial');
        $estabelecimento->cnpj = $request->get('cnpj');
        $estabelecimento->logradouro = $request->get('logradouro');
        $estabelecimento->numero = $request->get('numero');
        $estabelecimento->complemento = $request->get('complemento');
        $estabelecimento->bairro = $request->get('bairro');
        $estabelecimento->uf = $request->get('uf');
        $estabelecimento->cep = $request->get('cep');
        $estabelecimento->telefone = $request->get('telefone');
        $estabelecimento->dependencia = $request->get('dependencia');
        $estabelecimento->regional_saude = $request->get('regional_saude');
        $estabelecimento->tipo = $request->get('tipo');
        $estabelecimento->subtipo = $request->get('subtipo');
        $estabelecimento->gestao = $request->get('gestao');

        $estabelecimento->save();

        return redirect()->route('admin.estabelecimentos.index');

    }


    public function destroy($id)
    {
        //
        $estabelecimento = Estabelecimento::findOrFail($id);

        foreach ($estabelecimento->triagems as $triagem) {
          # code...
          foreach ($triagem->evolucoes as $evolucao) {
            # code...
            $evolucao->delete();
          }

          foreach ($triagem->usos as $uso) {
            # code...
            $uso->delete();
          }

          $triagem->delete();
        }

        foreach ($estabelecimento->pacientes as $paciente) {
          # code...
          $paciente->delete();
        }

        foreach ($estabelecimento->users as $user) {
          # code...
          $user->delete();
        }

        $estabelecimento->delete();

        return redirect()->route('admin.estabelecimentos.index');
    }
}
