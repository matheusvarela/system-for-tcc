<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Substancia extends Model
{

  protected $fillable = ['nome'];

  protected $guarded = ['id', 'created_at', 'updated_at'];

  protected $table = 'substancias';

  public $timestamps = false;

  public function usos()
  {
    return $this->hasMany('App\Uso');
  }

}


?>
