<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estabelecimento extends Model
{
    //

    protected $fillable = ['cnes', 'nome', 'municipio', 'natureza_juridica', 'nome_empresarial', 'cnpj', 'logradouro', 'numero', 'complemento', 'bairro', 'uf', 'cep', 'telefone', 'dependencia','regional_saude', 'tipo', 'subtipo', 'gestao', 'administrador'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $table = 'estabelecimentos';

    public function pacientes()
    {
      return $this->hasMany('App\Paciente');
    }

    public function users()
    {
      return $this->hasMany('App\User');
    }

    public function triagems()
    {
      return $this->hasMany('App\Triagem');
    }
}
