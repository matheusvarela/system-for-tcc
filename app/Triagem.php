<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Triagem extends Model
{
    //

    protected $fillable = ['data', 'espontanea', 'encaminhado', 'etaria', 'deficiencia', 'cid', 'psf', 'fora_area',
                            'motivacao_buscar', 'ultima_vez', 'doenca_familiar', 'parar_sem_ajuda', 'estrategias', 'trat_anterior',
                            'vezes', 'local', 'problemas_justica_policia', 'paciente_id', 'estabelecimento_id', 'user_id'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $table = 'triagems';

    public function pacientes()
    {
      # code...
      return $this->belongsTo('App\Paciente', 'paciente_id');
    }

    public function estabelecimentos()
    {
      # code...
      return $this->belongsTo('App\Estabelecimento', 'estabelecimento_id');
    }

    public function users()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

    public function usos()
    {
      return $this->hasMany('App\Uso');
    }

    public function evolucoes()
    {
      return $this->hasMany('App\Evolucao');
    }
}
