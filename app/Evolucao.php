<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evolucao extends Model
{
    //

    protected $fillable = ['data', 'historico', 'diagnostico', 'conduta', 'user_id', 'triagem_id'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $table = 'evolucoes';

    public function triagems()
    {
      # code...
      return $this->belongsTo('App\Triagem', 'triagem_id');
    }

    public function users()
    {
      return $this->belongsTo('App\User', 'user_id');
    }
}
