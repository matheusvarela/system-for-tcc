<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uso extends Model
{

  protected $fillable = ['substancia_id', 'tempo_uso', 'freq_uso', 'max_freq_uso', 'triagem_id'];

  protected $guarded = ['id', 'created_at', 'updated_at'];

  protected $table = 'usos';

  public $timestamps = false;

  public function substancias()
  {
    return $this->belongsTo('App\Substancia');
  }

  public function triagens()
  {
    return $this->belongsTo('App\Triagem');
  }
}

 ?>
