<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    //

    protected $fillable = ['nome', 'apelido', 'data_nascimento', 'idade', 'sexo',
                            'nome_mae', 'nome_pai', 'cns', 'cpf', 'rg', 'data_expedicao', 'naturalidade',
                            'estado_civil', 'filhos', 'escolaridade', 'etnia', 'reside_com', 'telefone',
                            'endereco', 'cep', 'referencia', 'profissao', 'local_trabalho', 'renda', 'estabelecimento_id'];

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $table = 'pacientes';

    public function triagems()
    {
      # code...
      return $this->hasMany('App\Triagem');
    }

    public function estabelecimentos()
    {
      return $this->belongsTo('App\Estabelecimento', 'estabelecimento_id');
    }
}
