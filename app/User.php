<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'estabelecimento_id', 'admin', 'cpf', 'matricula', 'funcao', 'cargo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function estabelecimentos()
    {
      return $this->belongsTo('App\Estabelecimento', 'estabelecimento_id');
    }

    public function triagems()
    {
      return $this->hasMany('App\Triagem');
    }

    public function evolucoes()
    {
      return $this->hasMany('App\Evolucao');
    }
}
