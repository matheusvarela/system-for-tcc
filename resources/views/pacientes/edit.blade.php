@extends('layouts.base')

@section('content')
<div class="container">
    <form class="" action="{{ action('PacienteController@update', $id) }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">

        <!-- Dados Pessoais -->

        <div class="bg-light border-left border-bottom rounded" id="dados_pessoais">
            <h3 class="border-gray border-bottom mx-3 my-3">Dados Pessoais</h3>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }} col-md-8">
                    <label for="nome">Nome Completo*</label>
                    <input type="text" name="nome" class="form-control form-control-sm" id="nome" placeholder="Nome do Paciente" required value="{{$paciente->nome}}">

                    @if ($errors->has('nome'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nome') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('apelido') ? ' has-error' : '' }} col-md-4">
                    <label for="apelido">Apelido</label>
                    <input type="text" name="apelido" class="form-control form-control-sm" id="apelido" placeholder="Apelido" value="{{$paciente->apelido}}">

                    @if ($errors->has('apelido'))
                        <span class="help-block">
                            <strong>{{ $errors->first('apelido') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('data_nascimento') ? ' has-error' : '' }} col-md-2">
                    <label for="data_nascimento">Data Nascimento*</label>
                    <input class="form-control form-control-sm" name="data_nascimento" value="{{$paciente->data_nascimento}}" required>

                    @if ($errors->has('data_nascimento'))
                        <span class="help-block">
                            <strong>{{ $errors->first('data_nascimento') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('idade') ? ' has-error' : '' }} col-md-2">
                    <label for="idade">Idade</label>
                    <input type="text" class="form-control form-control-sm" id="idade" name="idade" value="{{$paciente->idade}}">

                    @if ($errors->has('idade'))
                        <span class="help-block">
                            <strong>{{ $errors->first('idade') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('sexo') ? ' has-error' : '' }} col-md-3">
                    <label for="sexo">Sexo</label>
                    <select class="form-control form-control-sm" id="sexo" name="sexo">
                        <option value="" selected>@if ($paciente->sexo) {{$paciente->sexo}} @else Escolha... @endif</option>
                        <option>Feminino</option>
                        <option>Masculino</option>
                    </select>

                    @if ($errors->has('sexo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sexo') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('nome_mae') ? ' has-error' : '' }} col-md-6">
                    <label for="nome">Mãe*</label>
                    <input type="text" name="nome_mae" class="form-control form-control-sm" id="nome_mae" placeholder="Nome da Mãe"value="{{$paciente->nome_mae}}" required>

                    @if ($errors->has('nome_mae'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nome_mae') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('nome_pai') ? ' has-error' : '' }} col-md-6">
                    <label for="nome">Pai</label>
                    <input type="text" name="nome_pai" class="form-control form-control-sm" id="nome_pai" placeholder="Nome da Pai" value="{{$paciente->nome_pai}}">

                    @if ($errors->has('nome_pai'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nome_pai') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('cns') ? ' has-error' : '' }} col-md-3">
                    <label for="cns">CNS*</label>
                    <input type="text" name="cns" id="cns" class="form-control form-control-sm" value="{{$paciente->cns}}" required>

                    @if ($errors->has('cns'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cns') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }} col-md-2">
                    <label for="cpf">CPF*</label>
                    <input type="text" name="cpf" id="cpf" class="form-control form-control-sm" maxlength="14" value="{{$paciente->cpf}}" required>

                    @if ($errors->has('cpf'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cpf') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('rg') ? ' has-error' : '' }} col-md-2">
                    <label for="rg">RG*</label>
                    <input type="text" name="rg" id="rg" class="form-control form-control-sm" value="{{$paciente->rg}}" required>

                    @if ($errors->has('rg'))
                        <span class="help-block">
                            <strong>{{ $errors->first('rg') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('data_expedicao') ? ' has-error' : '' }} col-md-2">
                    <label for="data_expedicao">Data de Expedição*</label>
                    <input name="data_expedicao" class="form-control form-control-sm" id="data_expedicao" value="{{$paciente->data_expedicao}}" required>

                    @if ($errors->has('data_expedicao'))
                        <span class="help-block">
                            <strong>{{ $errors->first('data_expedicao') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('naturalidade') ? ' has-error' : '' }} col-md-3">
                    <label for="naturalidade">Naturalidade</label>
                    <input type="text" name="naturalidade" id="naturalidade" class="form-control form-control-sm" value="{{$paciente->naturalidade}}">

                    @if ($errors->has('naturalidade'))
                        <span class="help-block">
                            <strong>{{ $errors->first('naturalidade') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('etnia') ? ' has-error' : '' }} col-md-2">
                    <label for="etnia">Etnia</label>
                    <input type="text" name="etnia" id="etnia" class="form-control form-control-sm" value="{{$paciente->etnia}}">

                    @if ($errors->has('etnia'))
                        <span class="help-block">
                            <strong>{{ $errors->first('etnia') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('estado_civil') ? ' has-error' : '' }} col-md-2">
                    <label for="estado_civil">Estado Civil</label>
                    <select class="form-control form-control-sm" name="estado_civil" id="estado_civil">
                        <option selected>@if ($paciente->estado_civil) {{$paciente->estado_civil}} @else Escolha... @endif</option>
                        <option>Solteiro</option>
                        <option>Casado</option>
                        <option>Divorciado</option>
                    </select>

                    @if ($errors->has('estado_civil'))
                        <span class="help-block">
                            <strong>{{ $errors->first('estado_civil') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('filhos') ? ' has-error' : '' }} col-md-2">
                    <label for="filhos">Número de filhos</label>
                    <input type="text" name="filhos" id="filhos" class="form-control form-control-sm" value="{{$paciente->filhos}}">

                    @if ($errors->has('filhos'))
                        <span class="help-block">
                            <strong>{{ $errors->first('filhos') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('escolaridade') ? ' has-error' : '' }} col-md-3">
                    <label for="escolaridade">Escolaridade</label>
                    <select class="form-control form-control-sm" name="escolaridade" id="escolaridade">
                        <option selected>@if ($paciente->escolaridade) {{$paciente->escolaridade}} @else Escolha... @endif</option>
                        <option>Fundamental - Incompleto</option>
                        <option>Fundamental - Completo</option>
                        <option>Médio - Incompleto</option>
                    </select>

                    @if ($errors->has('escolaridade'))
                        <span class="help-block">
                            <strong>{{ $errors->first('escolaridade') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }} col-md-3">
                    <label for="telefone">Telefone</label>
                    <input type="text" class="form-control form-control-sm" name="telefone" id="telefone" maxlength="11" value="{{$paciente->telefone}}">

                    @if ($errors->has('telefone'))
                    <span class="help-block">
                      <strong>{{ $errors->first('telefone') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
        </div>

        <!-- Fim dados Pessoais -->

        <!-- Dados Residenciais -->

        <div class="bg-light border-left border-bottom rounded" id="dados_residenciais">
            <h3 class="border-gray border-bottom mx-3 my-3">Dados Residenciais</h3>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('endereco') ? ' has-error' : '' }} col-md-8">
                    <label for="endereco">Endereço</label>
                    <input type="text" class="form-control form-control-sm" name="endereco" id="endereco" value="{{$paciente->endereco}}">

                    @if ($errors->has('endereco'))
                        <span class="help-block">
                            <strong>{{ $errors->first('endereco') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }} col-md-2">
                    <label for="cep">CEP*</label>
                    <input type="text" name="cep" id="cep" class="form-control form-control-sm" maxlength="8" value="{{$paciente->cep}}">

                    @if ($errors->has('cep'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cep') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('referencia') ? ' has-error' : '' }} col-md-6">
                    <label for="referencia">Ponto de referência</label>
                    <input type="text" class="form-control form-control-sm" name="referencia" id="referencia" value="{{$paciente->referencia}}">

                    @if ($errors->has('referencia'))
                        <span class="help-block">
                            <strong>{{ $errors->first('referencia') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('reside_com') ? ' has-error' : '' }} col-md-4">
                    <label for="reside_com">Com quem reside</label>
                    <input type="text" name="reside_com" id="reside_com" class="form-control form-control-sm" value="{{$paciente->reside_com}}">

                    @if ($errors->has('reside_com'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reside_com') }}</strong>
                        </span>
                    @endif
                </div>

            </div>
        </div>

        <!-- Fim dos dados Residenciais -->

        <!-- Dados Profissionais -->

        <div class="bg-light border-left border-bottom rounded" id="dados_profissionais">
            <h3 class="border-gray border-bottom mx-3 my-3">Dados Profissionais</h3>
            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('profissao') ? ' has-error' : '' }} col-md-4">
                    <label for="profissao">Profissão</label>
                    <input type="text" name="profissao" class="form-control form-control-sm" id="profissao" value="{{$paciente->profissao}}">

                    @if ($errors->has('telefone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telefone') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('local_trabalho') ? ' has-error' : '' }} col-md-4">
                    <label for="local_trabalho">Local de trabalho</label>
                    <input type="text" name="local_trabalho" class="form-control form-control-sm" id="local_trabalho" value="{{$paciente->local_trabalho}}">

                    @if ($errors->has('local_trabalho'))
                        <span class="help-block">
                            <strong>{{ $errors->first('local_trabalho') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('renda') ? ' has-error' : '' }} col-md-4">
                    <label for="renda">Renda</label>
                    <select class="form-control form-control-sm" id="renda" name="renda">
                        <option selected>@if ($paciente->renda) {{$paciente->renda}} @else Escolha... @endif</option>
                        <option>Não possui renda</option>
                        <option>1/2 até 1 salário mínimo</option>
                        <option>2 até 3 salários mínimos</option>
                        <option>4 até 5 salários mínimos</option>
                        <option>5 salários mínimos ou mais</option>
                        <option>Não informado</option>
                    </select>

                    @if ($errors->has('renda'))
                        <span class="help-block">
                            <strong>{{ $errors->first('renda') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <!-- Fim dos dados Profissionais -->

        <div class="form-group mx-3 my-3">
          <button type="submit" class="btn btn-secondary btn-sm"  name="atualizar" value="">Atualizar</button>
        </div>


    </form>
</div>
@endsection
