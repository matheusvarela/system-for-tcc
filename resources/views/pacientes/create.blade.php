@extends('layouts.base')

@section('content')
<div class="container">
  <form class="" action="{{ url('pacientes') }}" method="post">
    {{ csrf_field() }}

    <!-- Dados pessoais -->
    <div class="bg-light border-left border-bottom rounded" id="dados_pessoais">
        <h3 class="border-gray border-bottom mx-3 my-3">Dados Pessoais</h3>
        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }} col-md-8">
                <label for="nome">Nome Completo*</label>
                <input type="text" name="nome" class="form-control form-control-sm" id="nome" value="{{ old('nome') }}" placeholder="Nome do Paciente" required >

                @if ($errors->has('nome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('apelido') ? ' has-error' : '' }} col-md-4">
                <label for="apelido">Apelido</label>
                <input type="text" name="apelido" class="form-control form-control-sm" id="apelido" value="{{ old('apelido') }}" placeholder="Apelido do Paciente">

                @if ($errors->has('apelido'))
                    <span class="help-block">
                        <strong>{{ $errors->first('apelido') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('data_nascimento') ? ' has-error' : '' }} col-md-2">
                <label for="data_nascimento">Data Nascimento*</label>
                <input class="form-control form-control-sm" id="data_nascimento" value="{{ old('data_nascimento') }}" name="data_nascimento" required>

                @if ($errors->has('data_nascimento'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data_nascimento') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('idade') ? ' has-error' : '' }} col-md-2">
                <label for="idade">Idade</label>
                <input class="form-control form-control-sm" id="idade" maxlength="2" value="{{ old('idade') }}" placeholder="Idade do Paciente" name="idade">

                @if ($errors->has('idade'))
                    <span class="help-block">
                        <strong>{{ $errors->first('idade') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('sexo') ? ' has-error' : '' }} col-md-2">
                <label for="sexo">Sexo</label>
                <select class="form-control form-control-sm" id="sexo" name="sexo">
                    <option value="{{ old('sexo') }}" selected>Escolha...</option>
                    <option>Feminino</option>
                    <option>Masculino</option>
                </select>

                @if ($errors->has('sexo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('sexo') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('nome_mae') ? ' has-error' : '' }} col-md-6">
                <label for="nome">Mãe*</label>
                <input type="text" name="nome_mae" class="form-control form-control-sm" id="nome_mae" value="{{ old('nome_mae') }}" placeholder="Nome da Mãe" required>

                @if ($errors->has('nome_mae'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome_mae') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('nome_pai') ? ' has-error' : '' }} col-md-6">
                <label for="nome">Pai</label>
                <input type="text" name="nome_pai" class="form-control form-control-sm" id="nome_pai" value="{{ old('nome_pai') }}" placeholder="Nome da Pai">

                @if ($errors->has('nome_pai'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome_pai') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('cns') ? ' has-error' : '' }} col-md-3">
                <label for="cns">CNS*</label>
                <input name="cns" id="cns" class="form-control form-control-sm" value="{{ old('cns') }}" placeholder="Cartão Nacional de Saúde" required>

                @if ($errors->has('cns'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cns') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }} col-md-2">
                <label for="cpf">CPF*</label>
                <input name="cpf" id="cpf" class="form-control form-control-sm" value="{{ old('cpf') }}" maxlength="14" required>

                @if ($errors->has('cpf'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cpf') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('rg') ? ' has-error' : '' }} col-md-2">
                <label for="rg">RG*</label>
                <input name="rg" id="rg" class="form-control form-control-sm" value="{{ old('rg') }}" placeholder="Registro Geral" required>

                @if ($errors->has('rg'))
                    <span class="help-block">
                        <strong>{{ $errors->first('rg') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('data_expedicao') ? ' has-error' : '' }} col-md-2">
                <label for="data_expedicao">Data de Expedição*</label>
                <input name="data_expedicao" class="form-control form-control-sm" value="{{ old('data_expedicao') }}" id="data_expedicao" required>

                @if ($errors->has('data_expedicao'))
                    <span class="help-block">
                        <strong>{{ $errors->first('data_expedicao') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('naturalidade') ? ' has-error' : '' }} col-md-3">
                <label for="naturalidade">Naturalidade</label>
                <input type="text" name="naturalidade" id="naturalidade" value="{{ old('naturalidade') }}" class="form-control form-control-sm" placeholder="Naturalidade">

                @if ($errors->has('naturalidade'))
                    <span class="help-block">
                        <strong>{{ $errors->first('naturalidade') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('etnia') ? ' has-error' : '' }} col-md-2">
                <label for="etnia">Etnia</label>
                <input type="text" name="etnia" id="etnia" value="{{ old('etnia') }}" class="form-control form-control-sm" placeholder="Etnia">

                @if ($errors->has('etnia'))
                    <span class="help-block">
                        <strong>{{ $errors->first('etnia') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('estado_civil') ? ' has-error' : '' }} col-md-2">
                <label for="estado_civil">Estado Civil</label>
                <select class="form-control form-control-sm" name="estado_civil" id="estado_civil">
                    <option value="{{ old('estado_civil') }}" selected>Escolha...</option>
                    <option>Solteiro</option>
                    <option>Casado</option>
                    <option>Divorciado</option>
                </select>

                @if ($errors->has('estado_civil'))
                    <span class="help-block">
                        <strong>{{ $errors->first('estado_civil') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('filhos') ? ' has-error' : '' }} col-md-2">
                <label for="filhos">Número de filhos</label>
                <input name="filhos" id="filhos" class="form-control form-control-sm" value="{{ old('filhos') }}" maxlength="2" placeholder="Número de filhos">

                @if ($errors->has('filhos'))
                    <span class="help-block">
                        <strong>{{ $errors->first('filhos') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('escolaridade') ? ' has-error' : '' }} col-md-2">
                <label for="escolaridade">Escolaridade</label>
                <select class="form-control form-control-sm" name="escolaridade" id="escolaridade">
                    <option value="{{ old('escolaridade') }}" selected>Escolha...</option>
                    <option>Fundamental - Incompleto</option>
                    <option>Fundamental - Completo</option>
                    <option>Médio - Incompleto</option>
                </select>

                @if ($errors->has('escolaridade'))
                    <span class="help-block">
                        <strong>{{ $errors->first('escolaridade') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }} col-md-4">
                <label for="telefone">Telefone</label>
                <input class="form-control form-control-sm" value="{{ old('telefone') }}" name="telefone" id="telefone" maxlength="11">

                @if ($errors->has('telefone'))
                <span class="help-block">
                  <strong>{{ $errors->first('telefone') }}</strong>
                </span>
                @endif
            </div>
        </div>
    </div>

    <!-- Fim dos dados pessoais -->

    <!-- Dados residenciais -->

    <div class="bg-light border-left border-bottom rounded" id="dados_residenciais">
        <h3 class="border-gray border-bottom mx-3 my-3">Dados Residenciais</h3>
        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('endereco') ? ' has-error' : '' }} col-md-8">
                <label for="endereco">Endereço</label>
                <input type="text" class="form-control form-control-sm" name="endereco" value="{{ old('endereco') }}" id="endereco" placeholder="Endereço completo">

                @if ($errors->has('endereco'))
                    <span class="help-block">
                        <strong>{{ $errors->first('endereco') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }} col-md-2">
                <label for="cep">CEP*</label>
                  <input name="cep" id="cep" class="form-control form-control-sm" value="{{ old('cep') }}" maxlength="8" required>

                  @if ($errors->has('cep'))
                      <span class="help-block">
                          <strong>{{ $errors->first('cep') }}</strong>
                      </span>
                  @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('referencia') ? ' has-error' : '' }} col-md-6">
                <label for="referencia">Ponto de referência</label>
                <input type="text" class="form-control form-control-sm" name="referencia" value="{{ old('referencia') }}" id="referencia" placeholder="Ponto de referencia">

                @if ($errors->has('referencia'))
                    <span class="help-block">
                        <strong>{{ $errors->first('referencia') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('reside_com') ? ' has-error' : '' }} col-md-4">
                <label for="reside_com">Com quem reside</label>
                <input type="text" name="reside_com" id="reside_com" class="form-control form-control-sm" value="{{ old('reside_com') }}" placeholder="Com quem reside">

                @if ($errors->has('reside_com'))
                    <span class="help-block">
                        <strong>{{ $errors->first('reside_com') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <!-- Fim dos dados residenciais -->

    <!-- Dados profissionais -->

    <div class="bg-light border-left border-bottom rounded" id="dados_profissionais">
        <h3 class="border-gray border-bottom mx-3 my-3">Dados Profissionais</h3>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('profissao') ? ' has-error' : '' }} col-md-4">
                <label for="profissao">Profissão</label>
                <input type="text" name="profissao" class="form-control form-control-sm" value="{{ old('profissao') }}" id="profissao" placeholder="Profissão">

                @if ($errors->has('profissao'))
                    <span class="help-block">
                        <strong>{{ $errors->first('profissao') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('local_trabalho') ? ' has-error' : '' }} col-md-4">
                <label for="local_trabalho">Local de trabalho</label>
                <input type="text" name="local_trabalho" class="form-control form-control-sm" id="local_trabalho" value="{{ old('local_trabalho') }}" placeholder="Local de trabalho">

                @if ($errors->has('local_trabalho'))
                    <span class="help-block">
                        <strong>{{ $errors->first('local_trabalho') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('renda') ? ' has-error' : '' }} col-md-4">
                <label for="renda">Renda</label>
                <select class="form-control form-control-sm" id="renda" name="renda">
                    <option value="{{ old('renda') }}" selected>Escolha...</option>
                    <option>Não possui renda</option>
                    <option>1/2 até 1 salário mínimo</option>
                    <option>2 até 3 salários mínimos</option>
                    <option>4 até 5 salários mínimos</option>
                    <option>5 salários mínimos ou mais</option>
                    <option>Não informado</option>
                </select>

                @if ($errors->has('renda'))
                    <span class="help-block">
                        <strong>{{ $errors->first('renda') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <!-- Fim dos dados profissionais -->

    <div class="form-group mx-3 my-3">
      <button type="submit" class="btn btn-primary btn-sm"  name="cadastrar">Registrar</button>
    </div>

  </form>
</div>
@endsection
