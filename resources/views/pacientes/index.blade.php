@extends('layouts.base')

@section('content')
<div class="container">
  <div class="top-left mx-3 my-3">
      <div class="btn-toolbar" role="toolbar">
          <div class="btn-group" role="group">
              <a href="{{action('PacienteController@create')}}" class="btn btn-success rounded btn-sm" data-toggle="tooltip" data-placement="bottom" title="Cadastrar paciente">Cadastrar</a>
          </div>
      </div>
  </div>
<form action="{{action('PacienteController@index')}}">
  {{ csrf_field() }}
  <nav class="navbar navbar-light bg-light mx-3 my-3">
    <input class="form-control mr-sm-2 col-md-6" name="search" type="search" placeholder="Consultar por Nome ou CPF ou CNS" aria-label="Search">
  </nav>
</form>

  <table class="table table-bordered table-responsive-sm table-hover table-sm text-center">
    <thead>
      <tr>
        <th scope="col" style="width: 20%;">Nome</th>
        <th scope="col">CPF</th>
        <th scope="col">Data de Nascimento</th>
        <th scope="col">CNS</th>
        <th scope="col">Ingressou</th>
        <th scope="col">Opções</th>
        <th scope="col">Triagem/Retriagem</th>
        <th scope="col">Evolução</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($pacientes as $paciente)
      <tr>
        <td>{{$paciente->nome}}</td>
        <td data-mask="000.000.000-00">{{$paciente->cpf}}</td>
        <td data-mask="00/00/0000">{{$paciente->data_nascimento}}</td>
        <td>{{$paciente->cns}}</td>
        <td>{{date('d/m/Y', strtotime($paciente->created_at))}}</td>
        <td>
          <a href="{{action('PacienteController@show', $paciente->id)}}" class="btn btn-outline-success btn-sm rounded mb-1">Visualizar</a>
          <a href="{{action('PacienteController@edit', $paciente->id)}}" class="btn btn-outline-secondary rounded btn-sm mb-1">Atualizar</a>
          @if (Auth::user()->admin)
              <form action="{{action('PacienteController@destroy', $paciente->id)}}" method="post">
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-outline-danger rounded btn-sm">Deletar</button>
              </form>
          @endif
        </td>
        <td>
            <a href="{{action('TriagemController@index', $paciente['id'])}}" class="btn btn-outline-success rounded btn-sm">Avançar</a>
        </td>
        <td>
            <a href="{{action('EvolucaoController@index', $paciente['id'])}}" class="btn btn-outline-success rounded btn-sm">Avançar</a>

        </td>
      </tr>
      @endforeach
    </tbody>
</table>
<div class="text-center">
  {{$pacientes->appends(Request::only('search'))->render("pagination::bootstrap-4")}}
</div>
</div>
@endsection
