@extends('layouts.base')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-left mt-5 mx-3">
        <div class="card-deck">
            <div class="card bg-light mb-3" style="max-width: 18rem;">
                <div class="card-header">Pacientes</div>
                    <div class="card-body">
                        <h5 class="card-title">Área pacientes</h5>
                            <p class="card-text">Acesso a área de informações, triagens/retriagens e evoluções dos pacientes.</p>
                                <a href="{{ url('/pacientes')}}" class="btn btn-primary">Avançar</a>
                    </div>
            </div>


            <div class="card bg-light mb-3" style="max-width: 18rem;">
                <div class="card-header">Funcionários</div>
                    <div class="card-body">
                        <h5 class="card-title">Área Funcionários</h5>
                            <p class="card-text">Acesso a área de informações, listagem e cadastro dos Funcionários.</p>
                                <a href="{{route('funcionario.index')}}" class="btn btn-primary">Avançar</a>
                    </div>
            </div>



        </div>
    </div>
</div>
@endsection
