@extends('layouts.base')

@section('content')
<div class="container">
  <form class="form-horizontal" method="POST" action="{{action('FuncionarioController@store')}}">
      {{ csrf_field() }}
      <div class="bg-light border-left border-bottom rounded" id="dados_estabelecimento">
          <h3 class="border-gray border-bottom mx-3 my-3">Dados do Funcionário</h3>
          <div class="form-row mx-3">
              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} col-md-6">
                <label for="name" class="control-label">Nome</label>
                    <input id="name" type="text" class="form-control form-control-sm" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
              </div>

              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-6">
                  <label for="email" class="control-label">Email</label>
                      <input id="email" type="email" class="form-control form-control-sm" name="email" value="{{ old('email') }}" required>

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
              </div>
          </div>

          <div class="form-row mx-3">
              <div class="form-group{{ $errors->has('cpf') ? ' has-error' : '' }} col-md-3">
                  <label for="cpf" class="control-label">CPF</label>
                      <input id="cpf" name="cpf" type="tel" class="form-control form-control-sm" value="{{ old('cpf') }}" maxlength="14" required autofocus>

                      @if ($errors->has('cpf'))
                          <span class="help-block">
                              <strong>{{ $errors->first('cpf') }}</strong>
                          </span>
                      @endif
              </div>

              <div class="form-group{{ $errors->has('matricula') ? ' has-error' : '' }} col-md-3">
                  <label for="matricula" class="control-label">Matrícula</label>
                      <input id="matricula" type="text" name="matricula" class="form-control form-control-sm" value="{{ old('matricula') }}" required autofocus>

                      @if ($errors->has('matricula'))
                          <span class="help-block">
                              <strong>{{ $errors->first('matricula') }}</strong>
                          </span>
                      @endif
              </div>

              <div class="form-group{{ $errors->has('cargo') ? ' has-error' : '' }} col-md-4">
                  <label for="cargo" class="control-label">Cargo</label>
                      <input id="cargo" type="text" name="cargo" class="form-control form-control-sm" value="{{ old('cargo') }}" required autofocus>

                      @if ($errors->has('cargo'))
                          <span class="help-block">
                              <strong>{{ $errors->first('cargo') }}</strong>
                          </span>
                      @endif
              </div>
          </div>

          <div class="form-row mx-3">
              <div class="form-group{{ $errors->has('funcao') ? ' has-error' : '' }} col-md-6">
                  <label for="funcao" class="control-label">Função</label>
                      <input id="funcao" type="text" name="funcao" class="form-control form-control-sm" value="{{ old('funcao') }}" required autofocus>

                      @if ($errors->has('funcao'))
                          <span class="help-block">
                              <strong>{{ $errors->first('funcao') }}</strong>
                          </span>
                      @endif
              </div>
              <div class="form-group col-md-6">
                  <label for="estabelecimento">Estabelecimento</label>
                  <input type="text" name="estabelecimento" id="estabelecimento" class="form-control form-control-sm" value="{{Auth::user()->estabelecimentos->nome}}" required readonly autofocus>
              </div>
          </div>

          <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-6">
              <label for="password" class="control-label">Senha</label>
                <input id="password" type="password" class="form-control form-control-sm" name="password" required>

                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-md-6">
              <label for="password-confirm" class="control-label">Confirme a senha</label>
                <input id="password-confirm" type="password" class="form-control form-control-sm" name="password_confirmation" required>
            </div>

          </div>
      </div>
      <div class="form-group mx-3 my-3">
          <div class="col-md-6 col-md-offset-4">
              <button type="submit"  class="btn btn-primary btn-sm">
                  Registrar
              </button>
          </div>
      </div>
  </form>
</div>
@endsection
