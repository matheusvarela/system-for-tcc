@extends('layouts.base')

@section('content')
<div class="container">
  <div class="top-left mx-3 my-3">
    @if (Auth::user()->admin)
    <div class="btn-toolbar" role="toolbar">
        <div class="btn-group" role="group">
            <a href="{{action('FuncionarioController@create')}}" class="btn btn-success rounded btn-sm" data-toggle="tooltip" data-placement="bottom" title="Cadastrar funcionário">Cadastrar</a>
        </div>
    </div>
    @endif
  </div>

  <form action="{{action('FuncionarioController@index')}}">
    {{ csrf_field() }}
    <nav class="navbar navbar-light bg-light mx-3 my-3">
      <input class="form-control mr-sm-2 col-md-6" name="search_func" type="search" placeholder="Consultar por Nome ou CPF ou Email ou Matrícula" aria-label="Search">
    </nav>
  </form>

  <table class="table table-bordered table-responsive-sm table-hover table-sm text-center">
    <thead>
      <tr>
        <th scope="col" style="width: 20%;">Funcionário</th>
        <th scope="col">CPF</th>
        <th scope="col">Email</th>
        <th scope="col">Matrícula</th>
        <th scope="col">Função</th>
        <th scope="col">Cargo</th>
        <th scope="col">Estabelecimento</th>
        <th scope="col">Opções</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($funcionarios as $funcionario)
      <tr>
        <td>{{$funcionario->name}}</td>
        <td data-mask="000.000.000-00">{{$funcionario->cpf}}</td>
        <td>{{$funcionario->email}}</td>
        <td>{{$funcionario->matricula}}</td>
        <td>{{$funcionario->funcao}}</td>
        <td>{{$funcionario->cargo}}</td>
        <td>{{$funcionario->estabelecimentos->nome}}</td>
        <td>
          <a href="{{action('FuncionarioController@show', $funcionario->id)}}" class="btn btn-outline-success rounded btn-sm mb-1">Visualizar</a>
          @if (Auth::user()->admin)
          <a href="{{action('FuncionarioController@edit', $funcionario->id)}}" class="btn btn-outline-secondary btn-sm mb-1">Editar</a>
              <form action="{{action('FuncionarioController@destroy', $funcionario->id)}}" method="post">
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-outline-danger rounded btn-sm">Deletar</button>
              </form>
          @endif
        </td>

      </tr>
      @endforeach
    </tbody>
</table>
<div class="text-center">
  {{$funcionarios->appends(Request::only('search_func'))->render("pagination::bootstrap-4")}}
</div>
</div>
@endsection
