<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>GPSM</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="system">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom border-gray">
        <a class="navbar-brand" href="{{action('HomeController@index')}}">GPSM</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav mr-auto">
            &nbsp;
          </ul>

          <ul class="nav navbar-nav navbar-right">

            <!-- Authentication Links -->
            @if (Auth::guest())
              <!--<li><a href="{{ route('login') }}" class="nav-link">Login</a></li>-->
              <li><a href="{{ url('admin/login') }}" class="nav-link">Área administrativa</a></li>
              <!--<li><a href="{{ route('register') }}" class="nav-link">Registrar</a></li>-->
            @else
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                  <span>Bem vindo, {{ Auth::user()->name }}</span>

                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" role="menu">
                  <li>
                    <a class="dropdown-item" href="{{action('FuncionarioController@edit', Auth::user()->id)}}">Alterar Perfil</a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="{{ url('/pacientes')}}">Área Pacientes</a>
                  </li>
                  @if(Auth::user()->admin == 1)
                  <li>
                    <a class="dropdown-item" href="{{route('funcionario.index')}}">Área Funcionários</a>
                  </li>
                  @endif
                  <li>
                      <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                              Sair
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                    </li>
                  </ul>
                </li>
                @endif
              </ul>
            </div>
          </nav>
          @yield('content')
        </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.14/jquery.mask.js"></script>
    <!--="{{ asset('js/jquery.mask.js') }}"></script>-->
    <script type="text/javascript">
      function function_psf() {
          var x = document.getElementById("fora_area").value;
          if (x == 0) {
            $("#field_psf").removeAttr('hidden');
            $("#field_psf").show();
          } else if (x == 1) {
            $("#field_psf").hide();
          }
      }

      function function_estrategia() {
          var x = document.getElementById("parar_sem_ajuda").value;
          if (x == 1) {
            $("#field_estrategias").removeAttr('hidden');
            $("#field_estrategias").show();
          } else if (x == 0) {
            $("#field_estrategias").hide();
          }
      }

      function function_vezesLocal() {
          var x = document.getElementById("trat_anterior").value;
          if (x == 1) {
            //vezes
            $("#field_vezes").removeAttr('hidden');
            $("#field_vezes").show();
            // local
            $("#field_local").removeAttr('hidden');
            $("#field_local").show();
          } else if (x == 0) {
            //vezes
            $("#field_vezes").hide();
            //local
            $("#field_local").hide();
          }
      }
    </script>
    <script>
      $(document).ready(function () {
        // adicionando mask ao cpf
        $("#cpf").attr('type', 'tel');
        $("#cpf").attr('placeholder', '___.___.___-__');
        $("#cpf").mask("999.999.999-99", {reverse: true});
        // adicionando mask ao Telefone
        $("#telefone").attr('type', 'tel');
        $("#telefone").attr('placeholder', '(__) _____-____');
        $("#telefone").mask('99 99999-9999', {reverse: true});

        // adicionando mask ao CEP
        $("#cep").attr('type', 'tel');
        $("#cep").attr('placeholder', '_____-___');
        $("#cep").mask("00000-000", {reverse: true});

        // adicionando mask a data_expedicao
        $("#data_expedicao").attr('type', 'tel');
        $("#data_expedicao").attr('placeholder', '__/__/____')
        $("#data_expedicao").mask("00/00/0000", {reverse: true});

        $("#data_nascimento").attr('type', 'tel');
        $("#data_nascimento").attr('placeholder', '__/__/____')
        $("#data_nascimento").mask("00/00/0000", {reverse: true});

        // adicionando attr a idade
        $("#idade").attr('type', 'tel');
        $("#idade").mask('99', {reverse: true});

        // adicionando mask CID
        $("#cid").mask('S-AA', {reverse: true});

        // adicionando attr tel a rg
        $("#rg").attr('type', 'tel');
        $("#rg").mask('0#');

        // adicionando attr tel a cns
        $("#cns").attr('type', 'tel');
        $("#cns").mask('0#');
        // adicionando attr tel a filhos
        $("#filhos").attr('type', 'tel');
        $("#filhos").mask('0#');

        // adicionando type tel em vezes
        $("#vezes").attr('type', 'tel');
        $("#vezes").mask('0#');
      });
    </script>
</body>
</html>
