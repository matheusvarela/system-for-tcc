@extends('layouts.base')

@section('content')
<div class="container">
  @if ($triagems->count() > 0)
  <div class="top-left mx-3 my-3">
      <div class="btn-toolbar" role="toolbar">
          <div class="btn-group" role="group">
              <a href="{{action('TriagemController@retriagem', $triagems->last()->id)}}" class="btn btn-secondary rounded btn-sm">Retriagem</a>
          </div>
      </div>
  </div>

  <table class="table table-bordered table-responsive-sm table-hover table-sm text-center">
    <thead>
      <tr>
        <th scope="col" style="width: 20%;">Responsável da Triagem</th>
        <th scope="col" style="width: 20%;">Paciente</th>
        <th scope="col">Código do CID</th>
        <th scope="col">Fora da área</th>
        <th scope="col">PSF</th>
        <th scope="col">Criado</th>
        <th scope="col">Atualizado</th>
        <th scope="col">Opções</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($triagems as $triagem)
        <tr>
          <td>{{$triagem->users->name}}</td>
          <td>{{$triagem->pacientes->nome}}</td>
          <td>{{$triagem->cid}}</td>
            @if (($triagem->fora_area == 0) && ($triagem->fora_area != NULL))
            <td>Não</td>
            <td>{{$triagem->psf != NULL ? $triagem->psf : 'Não informado'}}</td>
            @elseif ($triagem->fora_area == 1)
            <td>Sim</td>
            <td></td>
            @else
            <td>Não informado</td>
            <td></td>
            @endif
          <td>{{$triagem->data}}</td>
          <td>{{date('d/m/Y', strtotime($triagem['updated_at']))}} às {{date('H:i:s', strtotime($triagem['updated_at']))}}</td>
          <td>
            <a href="{{action('TriagemController@show', $triagem->id)}}" class="btn btn-outline-success btn-sm rounded mb-1">Visualizar</a>
            <a href="{{action('TriagemController@edit', $triagem->id)}}" class="btn btn-outline-secondary btn-sm rounded">Editar</a>
          </td>
        </tr>
        @endforeach
  @else
    <div class="top-left mx-3 my-3">
        <div class="btn-toolbar" role="toolbar">
            <div class="btn-group" role="group">
                <a href="{{action('TriagemController@create', $id)}}" class="btn btn-success rounded btn-sm" data-toggle="tooltip" data-placement="bottom" title="Cadastrar Triagem">Cadastrar</a>
            </div>
        </div>
    </div>
    <div class="alert alert-danger mx-3 col-md-4" role="alert">
        O paciente não possui triagem cadastrada!
    </div>
  @endif
    </tbody>
  </table>

  {{$triagems->render("pagination::bootstrap-4")}}
@endsection
