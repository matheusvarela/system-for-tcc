@extends('layouts.base')

@section('content')
<div class="container">
    <form class="" action="{{ action('TriagemController@store', $paciente['id']) }}" method="post">
    {{ csrf_field() }}

        <div class="bg-light border-left border-bottom rounded">
            <h3 class="border-gray border-bottom my-3 mx-3">Triagem</h3>

            <div class="form-row mx-3">
                <div class="form-group col-md-8">
                    <label for="">Paciente</label>
                    <input type="text" class="form-control form-control-sm" name="nome" value="{{$paciente->nome}}" readonly>
                </div>

                <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }} col-md-2">
                    <label for="">Data</label>
                    <input type="text" class="form-control form-control-sm" name="data" id="data" value="{{ date('d/m/Y') }}" readonly>

                    @if ($errors->has('data'))
                        <span class="help-block">
                            <strong>{{ $errors->first('data') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group col-md-8">
                    <label for="entrevistador">Entrevistador</label>
                    <input type="text" name="entrevistador" value="{{ Auth::user()->name }}" id="entrevistador" class="form-control form-control-sm" required readonly>
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('espontanea') ? ' has-error' : '' }} col-md-4">
                    <label for="espontanea">Buscou o tratamento espontaneamente?</label>
                    <select id="espontanea" name="espontanea" class="form-control form-control-sm">
                        <option value="" selected>Escolha...</option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>

                    @if ($errors->has('espontanea'))
                        <span class="help-block">
                            <strong>{{ $errors->first('espontanea') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('encaminhado') ? ' has-error' : '' }} col-md-6">
                    <label for="encaminhado">Foi encaminhado(a) de onde?</label>
                    <input type="text" name="encaminhado" id="encaminhado" value="{{ old('encaminhado') }}" class="form-control form-control-sm" >

                    @if ($errors->has('encaminhado'))
                        <span class="help-block">
                            <strong>{{ $errors->first('encaminhado') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('etaria') ? ' has-error' : '' }} col-md-2">
                    <label for="deficiencia">Faixa Etária</label>
                    @if (($paciente->idade) >= 16 && ($paciente->idade) <= 24)
                    <input type="text" name="etaria" class="form-control form-control-sm" id="etaria" value="16 a 24 anos" readonly>
                    @elseif (($paciente->idade) >= 25 && ($paciente->idade) <= 33)
                    <input type="text" name="etaria" class="form-control form-control-sm" id="etaria" value="25 a 33 anos" readonly>
                    @elseif (($paciente->idade) >= 34 && ($paciente->idade) <= 42)
                    <input type="text" name="etaria" class="form-control form-control-sm" id="etaria" value="34 a 42 anos" readonly>
                    @elseif (($paciente->idade) >= 43 && ($paciente->idade) <= 49)
                    <input type="text" name="etaria" class="form-control form-control-sm" id="etaria" value="43 a 49 anos" readonly>
                    @elseif (($paciente->idade) >= 50)
                    <input type="text" name="etaria" class="form-control form-control-sm" id="etaria" value="50 anos ou mais" readonly>
                    @else
                    <input type="text" name="etaria" class="form-control form-control-sm" id="etaria" value="Não informado" readonly>
                    @endif

                    @if ($errors->has('etaria'))
                        <span class="help-block">
                            <strong>{{ $errors->first('etaria') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('deficiencia') ? ' has-error' : '' }} col-md-2">
                    <label for="deficiencia">Deficiência</label>
                    <input type="text" name="deficiencia" value="{{ old('deficiencia') }}" class="form-control form-control-sm" id="deficiencia">

                    @if ($errors->has('deficiencia'))
                        <span class="help-block">
                            <strong>{{ $errors->first('deficiencia') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('cid') ? ' has-error' : '' }} col-md-3">
                    <label for="cid">Código do CID*</label>
                    <input type="text" name="cid" id="cid" value="{{ old('cid') }}" class="form-control form-control-sm" maxlength="3" required>

                    @if ($errors->has('cid'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cid') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('fora_area') ? ' has-error' : '' }} col-md-2">
                  <label for="fora_area">Fora da área</label>
                  <!--<input type="text" name="fora_area" class="form-control form-control-sm" id="fora_area">-->
                  <select class="form-control form-control-sm" name="fora_area" id="fora_area" onchange="function_psf()">
                    <option value="" selected>Escolha...</option>
                    <option value="0">Não</option>
                    <option value="1">Sim</option>
                  </select>

                  @if ($errors->has('fora_area'))
                      <span class="help-block">
                          <strong>{{ $errors->first('fora_area') }}</strong>
                      </span>
                  @endif
                </div>
                <div id="field_psf" class="form-group{{ $errors->has('psf') ? ' has-error' : '' }} col-md-3" hidden>
                    <label for="psf">Atendido pelo PSF</label>
                    <input type="text" name="psf" id="psf" class="form-control form-control-sm" value="{{ old('psf') }}">

                    @if ($errors->has('psf'))
                        <span class="help-block">
                            <strong>{{ $errors->first('psf') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('motivacao_buscar') ? ' has-error' : '' }} col-md-10">
                    <label for="motivacao_buscar">Motivo em buscar o tratamento</label>
                    <textarea name="motivacao_buscar" rows="3" class="form-control form-control-sm" id="motivacao_buscar" maxlength="255" cols="80">{{old('motivacao_buscar')}}</textarea>

                    @if ($errors->has('motivacao_buscar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('motivacao_buscar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Tabela uso de drogas -->

            <div class="table-responsive-sm mx-3">
                <table class="table table-hover table-bordered table-sm">
                    <thead>
                        <tr class="text-center">
                            <th scope="col">Substância</th>
                            <th scope="col">Tempo de uso</th>
                            <th scope="col">Frenquência de uso</th>
                            <th scope="col">Horário de maior frequência de uso</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr class="text-center">
                        <th scope="row">Tabaco</th>
                        <td> <input type="text" name="tab_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="tab_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="tab_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                      <tr class="text-center">
                        <th scope="row">Álcool</th>
                        <td> <input type="text" name="alc_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="alc_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="alc_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                      <tr class="text-center">
                        <th scope="row">Maconha</th>
                        <td> <input type="text" name="mac_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="mac_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="mac_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                      <tr class="text-center">
                        <th scope="row">Crack</th>
                        <td> <input type="text" name="cra_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="cra_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="cra_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                      <tr class="text-center">
                        <th scope="row">Mesclado</th>
                        <td> <input type="text" name="mes_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="mes_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="mes_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                      <tr class="text-center">
                        <th scope="row">Bebinho</th>
                        <td> <input type="text" name="beb_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="beb_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="beb_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                      <tr class="text-center">
                        <th scope="row">Cocaína</th>
                        <td> <input type="text" name="coc_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="coc_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="coc_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                      <tr class="text-center">
                        <th scope="row" >Injetáveis</th>
                        <td> <input type="text" name="inj_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="inj_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="inj_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                      <tr>
                        <td> <input type="text" name="subst_outras" class="form-control form-control-sm text-center" placeholder="Outras"/> </td>
                        <td> <input type="text" name="out_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="out_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="out_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>

                    </tbody>
                </table>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('ultima_vez') ? ' has-error' : '' }} col-md-2">
                    <label for="ultima_vez">Última vez que usou</label>
                    <input type="text" name="ultima_vez" value="{{ old('ultima_vez') }}" class="form-control form-control-sm" id="ultima_vez">

                    @if ($errors->has('ultima_vez'))
                        <span class="help-block">
                            <strong>{{ $errors->first('ultima_vez') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('doenca_familiar') ? ' has-error' : '' }} col-md-7">
                    <label for="doenca_familiar">Doenças familiares</label>
                    <input type="text" name="doenca_familiar" class="form-control form-control-sm" value="{{ old('doenca_familiar') }}" id="doenca_familiar" >

                    @if ($errors->has('doenca_familiar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('doenca_familiar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('parar_sem_ajuda') ? ' has-error' : '' }} col-md-3">
                    <label for="parar_sem_ajuda">Tentou parar sem ajuda ?</label>
                    <select class="form-control form-control-sm" name="parar_sem_ajuda" id="parar_sem_ajuda" onchange="function_estrategia()">
                        <option value="" selected>Escolha...</option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>

                    @if ($errors->has('parar_sem_ajuda'))
                        <span class="help-block">
                            <strong>{{ $errors->first('parar_sem_ajuda') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="field_estrategias" class="form-group{{ $errors->has('estrategias') ? ' has-error' : '' }} col-md-7" hidden>
                    <label for="estrategias">Estratégias utilizadas</label>
                    <input type="text" name="estrategias" class="form-control form-control-sm" id="estrategias">

                    @if ($errors->has('estrategias'))
                        <span class="help-block">
                            <strong>{{ $errors->first('estrategias') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('trat_anterior') ? ' has-error' : '' }} col-md-3">
                    <label for="trat_anterior">Tratamento anteriores</label>
                    <select class="form-control form-control-sm" id="trat_anterior" name="trat_anterior" onchange="function_vezesLocal()">
                        <option value="" selected>Escolha...</option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>

                    @if ($errors->has('trat_anterior'))
                        <span class="help-block">
                            <strong>{{ $errors->first('trat_anterior') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="field_vezes" class="form-group{{ $errors->has('vezes') ? ' has-error' : '' }} col-md-2" hidden>
                    <label for="vezes">Número de vezes</label>
                    <input type="tel" name="vezes" class="form-control form-control-sm" value="{{ old('vezes') }}" maxlength="3" id="vezes">

                    @if ($errors->has('vezes'))
                        <span class="help-block">
                            <strong>{{ $errors->first('vezes') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="field_local" class="form-group{{ $errors->has('local') ? ' has-error' : '' }} col-md-5" hidden>
                    <label for="local">Local onde se tratou</label>
                    <input type="text" name="local" class="form-control form-control-sm" id="local">

                    @if ($errors->has('local'))
                        <span class="help-block">
                            <strong>{{ $errors->first('local') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('problemas_justica_policia') ? ' has-error' : '' }} col-md-4">
                  <label for="problemas_justica_policia">Problemas com a Justiça e/ou Polícia</label>
                  <select class="form-control form-control-sm" id="problemas_justica_policia" name="problemas_justica_policia">
                      <option value="" selected>Escolha...</option>
                      <option value="0">Não</option>
                      <option value="1">Sim</option>
                  </select>

                  @if ($errors->has('problemas_justica_policia'))
                      <span class="help-block">
                          <strong>{{ $errors->first('problemas_justica_policia') }}</strong>
                      </span>
                  @endif
                </div>
            </div>

        </div>

        <div class="form-group mx-3 my-3">
            <button type="submit" class="btn btn-primary btn-sm"  name="cadastrar" value="">Registrar</button>
        </div>
    </form>
</div>
@endsection
