@extends('layouts.base')

@section('content')
<div class="container">
    <form class="" action="{{ action('TriagemController@store', $triagem->paciente_id) }}" method="post">
    {{ csrf_field() }}

        <div class="bg-light border-left border-bottom rounded">
            <h3 class="border-gray border-bottom my-3 mx-3">Retriagem</h3>

            <div class="form-row mx-3">
                <div class="form-group col-md-8">
                    <label for="">Paciente</label>
                    <input type="text" class="form-control form-control-sm" name="nome" value="{{$paciente}}" readonly>
                </div>

                <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }} col-md-2">
                    <label for="">Data</label>
                    <input type="text" class="form-control form-control-sm" name="data" id="data" value="{{ date('d/m/Y') }}" readonly>

                    @if ($errors->has('data'))
                        <span class="help-block">
                            <strong>{{ $errors->first('data') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group col-md-8">
                    <label for="entrevistador">Entrevistador</label>
                    <input type="text" name="entrevistador" value="{{ Auth::user()->name }}" id="entrevistador" class="form-control form-control-sm" required readonly>
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('espontanea') ? ' has-error' : '' }} col-md-4">
                    <label for="espontanea">Buscou o tratamento espontaneamente?</label>
                    <select id="espontanea" name="espontanea" class="form-control form-control-sm">
                        <option value="" selected>@if ($triagem->espontanea) {{$triagem->espontanea}} @else Escolha... @endif</option>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>

                    @if ($errors->has('espontanea'))
                        <span class="help-block">
                            <strong>{{ $errors->first('espontanea') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('encaminhado') ? ' has-error' : '' }} col-md-6">
                    <label for="encaminhado">Foi encaminhado(a) de onde?</label>
                    <input type="text" name="encaminhado" id="encaminhado" class="form-control form-control-sm" value="{{$triagem->encaminhado}}">

                    @if ($errors->has('encaminhado'))
                        <span class="help-block">
                            <strong>{{ $errors->first('encaminhado') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('etaria') ? ' has-error' : '' }} col-md-2">
                    <label for="etaria">Faixa Etária</label>
                      @if ($triagem->etaria)
                      <input type="text" name="etaria" class="form-control form-control-sm" id="etaria" value="{{$triagem->etaria}}" readonly>
                      @else
                      <input type="text" name="etaria" class="form-control form-control-sm" id="etaria" value="Não informado" readonly>
                      @endif

                      @if ($errors->has('etaria'))
                          <span class="help-block">
                              <strong>{{ $errors->first('etaria') }}</strong>
                          </span>
                      @endif
                </div>
                <div class="form-group{{ $errors->has('deficiencia') ? ' has-error' : '' }} col-md-2">
                    <label for="deficiencia">Deficiência</label>
                    <input type="text" name="deficiencia" class="form-control form-control-sm" id="deficiencia" value="{{$triagem->deficiencia}}">

                    @if ($errors->has('deficiencia'))
                        <span class="help-block">
                            <strong>{{ $errors->first('deficiencia') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('cid') ? ' has-error' : '' }} col-md-3">
                    <label for="cid">Código do CID</label>
                    <input type="text" name="cid" id="cid" class="form-control form-control-sm" maxlength="3" value="{{$triagem->cid}}" required>

                    @if ($errors->has('cid'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cid') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('fora_area') ? ' has-error' : '' }} col-md-2">
                    <label for="fora_area">Fora da área</label>
                    <select class="form-control form-control-sm" name="fora_area" id="fora_area" onchange="function_psf()">
                      @if (($triagem->fora_area == 0) && ($triagem->fora_area != NULL))
                      <option value="0" selected>Não</option>
                      @elseif ($triagem->fora_area == 1)
                      <option value="1" selected>Sim</option>
                      @else
                      <option value="" selected>Escolha...</option>
                      @endif
                      <option value="0">Não</option>
                      <option value="1">Sim</option>
                    </select>

                    @if ($errors->has('fora_area'))
                        <span class="help-block">
                            <strong>{{ $errors->first('fora_area') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="field_psf" class="form-group{{ $errors->has('psf') ? ' has-error' : '' }} col-md-3" {{$triagem->fora_area == 0 && $triagem->fora_area != NULL ? '' : 'hidden'}}>
                  <label for="psf">Atendido pelo PSF</label>
                  <input type="text" name="psf" id="psf" class="form-control form-control-sm" value="{{$triagem->psf}}">

                  @if ($errors->has('psf'))
                      <span class="help-block">
                          <strong>{{ $errors->first('psf') }}</strong>
                      </span>
                  @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('motivacao_buscar') ? ' has-error' : '' }} col-md-10">
                    <label for="motivacao_buscar">Motivo em buscar o tratamento</label>
                    <textarea name="motivacao_buscar" rows="3" class="form-control form-control-sm" maxlength="255" id="motivacao_buscar" cols="80">{{$triagem->motivacao_buscar}}</textarea>

                    @if ($errors->has('motivacao_buscar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('motivacao_buscar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <!-- Tabela uso de drogas -->

            <div class="table-responsive-sm mx-3">
                <table class="table table-hover table-bordered table-sm">
                    <thead>
                        <tr class="text-center">
                            <th scope="col">Substância</th>
                            <th scope="col">Tempo de uso</th>
                            <th scope="col">Frenquência de uso</th>
                            <th scope="col">Horário de maior frequência de uso</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($usos as $uso)

                      <tr class="text-center">
                        @if ($uso->substancia_id <= 8)
                        <th id="{{$uso->id}}" scope="row">{{$uso->nome}}</th>
                        <td> <input type="text" name="{{ ($uso->nome == 'Álcool' ? 'alc' : strtolower(mb_substr($uso->nome,0,3))) }}_tempo_uso" class="form-control form-control-sm" value="{{ $uso->tempo_uso }}"/> </td>
                        <td> <input type="text" name="{{ ($uso->nome == 'Álcool' ? 'alc' : strtolower(mb_substr($uso->nome,0,3))) }}_freq_uso" class="form-control form-control-sm" value="{{ $uso->freq_uso }}"/> </td>
                        <td> <input type="text" name="{{ ($uso->nome == 'Álcool' ? 'alc' : strtolower(mb_substr($uso->nome,0,3))) }}_max_freq_uso" class="form-control form-control-sm" value="{{ $uso->max_freq_uso }}"/> </td>
                        @else
                        <th> <input type="text" class="form-control form-control-sm text-center" name="outras" data-toggle="tooltip" data-placement="top" title="Substância adicionada" value="{{$uso->nome}}" readonly/> </th>
                        <td> <input type="text" name="out_tempo_uso" class="form-control form-control-sm" value="{{ $uso->tempo_uso }}"/> </td>
                        <td> <input type="text" name="out_freq_uso" class="form-control form-control-sm" value="{{ $uso->freq_uso }}"/> </td>
                        <td> <input type="text" name="out_max_freq_uso" class="form-control form-control-sm" value="{{ $uso->max_freq_uso }}"/> </td>
                        @endif

                      </tr>
                      @endforeach

                      @foreach ($list_not_subs as $subs)

                      <tr class="text-center">
                        @if ($subs->id <= 8)
                        <th scope="row">{{$subs->nome}}</th>
                        <td> <input type="text" name="{{ ($subs->nome == 'Álcool' ? 'alc' : strtolower(mb_substr($subs->nome,0,3))) }}_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="{{ ($subs->nome == 'Álcool' ? 'alc' : strtolower(mb_substr($subs->nome,0,3))) }}_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="{{ ($subs->nome == 'Álcool' ? 'alc' : strtolower(mb_substr($subs->nome,0,3))) }}_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        @endif
                      </tr>
                      @endforeach

                      <tr class="text-center">
                        <th> <input type="text" class="form-control form-control-sm text-center" name="out_nova" value="" placeholder="Outras"/> </th>
                        <td> <input type="text" name="out_nova_tempo_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="out_nova_freq_uso" class="form-control form-control-sm" value=""/> </td>
                        <td> <input type="text" name="out_nova_max_freq_uso" class="form-control form-control-sm" value=""/> </td>
                      </tr>


                    </tbody>
                </table>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('ultima_vez') ? ' has-error' : '' }} col-md-2">
                    <label for="ultima_vez">Última vez que usou</label>
                    <input type="text" name="ultima_vez" class="form-control form-control-sm" id="ultima_vez" value="{{$triagem->ultima_vez}}">

                    @if ($errors->has('ultima_vez'))
                        <span class="help-block">
                            <strong>{{ $errors->first('ultima_vez') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('doenca_familiar') ? ' has-error' : '' }} col-md-7">
                    <label for="doenca_familiar">Doenças familiares</label>
                    <input type="text" name="doenca_familiar" class="form-control form-control-sm" id="doenca_familiar" value="{{$triagem->doenca_familiar}}">

                    @if ($errors->has('doenca_familiar'))
                        <span class="help-block">
                            <strong>{{ $errors->first('doenca_familiar') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('parar_sem_ajuda') ? ' has-error' : '' }} col-md-3">
                    <label for="parar_sem_ajuda">Tentou parar sem ajuda ?</label>
                    <select class="form-control form-control-sm" name="parar_sem_ajuda" id="parar_sem_ajuda" onchange="function_estrategia()">
                        @if (($triagem->parar_sem_ajuda == 0) && ($triagem->parar_sem_ajuda != NULL))
                        <option value="0" selected>Não</option>
                        @elseif ($triagem->parar_sem_ajuda == 1)
                        <option value="1" selected>Sim</option>
                        @else
                        <option value="" selected>Escolha...</option>
                        @endif
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>

                    @if ($errors->has('parar_sem_ajuda'))
                        <span class="help-block">
                            <strong>{{ $errors->first('parar_sem_ajuda') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="field_estrategias" class="form-group{{ $errors->has('estrategias') ? ' has-error' : '' }} col-md-7" {{$triagem->parar_sem_ajuda == 0 || $triagem->parar_sem_ajuda == NULL ? 'hidden' : ''}}>
                    <label for="estrategias">Estratégias utilizadas</label>
                    <input type="text" name="estrategias" class="form-control form-control-sm" id="estrategias" value="{{$triagem->estrategias}}">

                    @if ($errors->has('estrategias'))
                        <span class="help-block">
                            <strong>{{ $errors->first('estrategias') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('trat_anterior') ? ' has-error' : '' }} col-md-3">
                    <label for="trat_anterior">Tratamento anteriores</label>
                    <select class="form-control form-control-sm" id="trat_anterior" name="trat_anterior" onchange="function_vezesLocal()">
                        @if (($triagem->trat_anterior == 0) && ($triagem->trat_anterior != NULL))
                        <option value="0" selected>Não</option>
                        @elseif ($triagem->trat_anterior == 1)
                        <option value="1" selected>Sim</option>
                        @else
                        <option value="" selected>Escolha...</option>
                        @endif
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>

                    @if ($errors->has('trat_anterior'))
                        <span class="help-block">
                            <strong>{{ $errors->first('trat_anterior') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="field_vezes" class="form-group{{ $errors->has('vezes') ? ' has-error' : '' }} col-md-2" {{$triagem->trat_anterior == 0 || $triagem->trat_anterior == NULL ? 'hidden' : ''}}>
                    <label for="vezes">Número de vezes</label>
                    <input type="tel" name="vezes" class="form-control form-control-sm" id="vezes" value="{{$triagem->vezes}}">

                    @if ($errors->has('vezes'))
                        <span class="help-block">
                            <strong>{{ $errors->first('vezes') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="field_local" class="form-group{{ $errors->has('local') ? ' has-error' : '' }} col-md-5" {{$triagem->trat_anterior == 0 || $triagem->trat_anterior == NULL ? 'hidden' : ''}}>
                    <label for="local">Local onde se tratou</label>
                    <input type="text" name="local" class="form-control form-control-sm" id="local" value="{{$triagem->local}}">

                    @if ($errors->has('local'))
                        <span class="help-block">
                            <strong>{{ $errors->first('local') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-row mx-3">
                <div class="form-group{{ $errors->has('problemas_justica_policia') ? ' has-error' : '' }} col-md-4">
                  <label for="problemas_justica_policia">Problemas com a Justiça e/ou Polícia</label>
                  <select class="form-control form-control-sm" id="problemas_justica_policia" name="problemas_justica_policia">
                      @if (($triagem->problemas_justica_policia == 0) && ($triagem->problemas_justica_policia != NULL))
                      <option value="0" selected>Não</option>
                      @elseif ($triagem->problemas_justica_policia == 1)
                      <option value="1" selected>Sim</option>
                      @else
                      <option value="" selected>Escolha...</option>
                      @endif
                      <option value="0">Não</option>
                      <option value="1">Sim</option>
                  </select>

                  @if ($errors->has('problemas_justica_policia'))
                      <span class="help-block">
                          <strong>{{ $errors->first('problemas_justica_policia') }}</strong>
                      </span>
                  @endif
                </div>
            </div>

        </div>

        <div class="form-group mx-3 my-3">
            <button type="submit" class="btn btn-secondary btn-sm"  name="atualizar" value="">Atualizar</button>
        </div>
    </form>
</div>
@endsection
