@extends('admin.layouts.base')

@section('content')
<div class="container">
  <div class="bg-light border-left border-bottom rounded" id="dados_estabelecimento">
      <h3 class="border-gray border-bottom mx-3 my-3">Dados do Estabelecimento</h3>
      <div class="form-row mx-3">
          <div class="form-group col-md-6">
              <label for="nome">Nome</label>
              <input type="text" name="nome" class="form-control form-control-sm" id="nome" value="{{$estabelecimento->nome}}" disabled >
          </div>
          <div class="form-group col-md-2">
              <label for="cnes">CNES</label>
              <input type="text" name="cnes" class="form-control form-control-sm" id="cnes" value="{{$estabelecimento->cnes}}" disabled>
          </div>

          <div class="form-group col-md-3">
              <label for="cnpj">CNPJ</label>
              <input type="text" name="cnpj" class="form-control form-control-sm" id="cnpj" value="{{$estabelecimento->cnpj}}" disabled>
          </div>
      </div>

      <div class="form-row mx-3">
          <div class="form-group col-md-6">
              <label for="nome_empresarial">Nome Empresarial</label>
              <input type="text" name="nome_empresarial" class="form-control form-control-sm" id="nome_empresarial" value="{{$estabelecimento->nome_empresarial}}" disabled>
          </div>
          <div class="form-group col-md-5">
              <label for="cnes">Natureza Jurídica (Grupo)</label>
              <input type="text" name="natureza_juridica" class="form-control form-control-sm" id="natureza_juridica" value="{{$estabelecimento->natureza_juridica}}" disabled>
          </div>
      </div>

      <div class="form-row mx-3">
          <div class="form-group col-md-6">
              <label for="logradouro">logradouro</label>
              <input type="text" name="logradouro" class="form-control form-control-sm" id="logradouro" value="{{$estabelecimento->logradouro}}" disabled>
          </div>
          <div class="form-group col-md-2">
              <label for="numero">Número</label>
              <input type="text" name="numero" class="form-control form-control-sm" id="numero" value="{{$estabelecimento->numero}}" disabled>
          </div>
          <div class="form-group col-md-3">
              <label for="complemento">Complemento</label>
              <input type="text" name="complemento" class="form-control form-control-sm" id="complemento" value="{{$estabelecimento->complemento}}" disabled>
          </div>
      </div>

      <div class="form-row mx-3">
          <div class="form-group col-md-4">
              <label for="bairro">Bairro</label>
              <input name="bairro" id="bairro" class="form-control form-control-sm" value="{{$estabelecimento->bairro}}" disabled>
          </div>
          <div class="form-group col-md-4">
              <label for="municipio">Município</label>
              <input name="municipio" id="municipio" class="form-control form-control-sm" value="{{$estabelecimento->municipio}}" disabled>
          </div>
          <div class="form-group col-md-2">
              <label for="uf">UF</label>
              <input name="uf" id="uf" class="form-control form-control-sm" maxlength="2" value="{{$estabelecimento->uf}}" disabled>
          </div>
      </div>

      <div class="form-row mx-3">
          <div class="form-group col-md-2">
              <label for="cep">CEP</label>
              <input type="text" name="cep" id="cep" class="form-control form-control-sm" maxlength="8" value="{{$estabelecimento->cep}}" disabled>
          </div>
          <div class="form-group col-md-2">
              <label for="telefone">Telefone</label>
              <input type="text" name="telefone" id="telefone" class="form-control form-control-sm" maxlength="14" value="{{$estabelecimento->telefone}}" disabled>
          </div>
          <div class="form-group col-md-2">
              <label for="dependencia">Dependência</label>
              <input type="text" name="dependencia" id="dependencia" class="form-control form-control-sm" value="{{$estabelecimento->dependencia}}" disabled>
          </div>
          <div class="form-group col-md-2">
              <label for="regional_saude">Regional de Saúde</label>
              <input type="text" name="regional_saude" id="regional_saude" class="form-control form-control-sm" value="{{$estabelecimento->regional_saude}}" disabled>
          </div>

          <div class="form-group col-md-2">
              <label for="gestao">Gestão</label>
              <input type="text" name="gestao" id="gestao" class="form-control form-control-sm" value="{{$estabelecimento->gestao}}" disabled>
          </div>
      </div>

      <div class="form-row mx-3">
          <div class="form-group col-md-4">
              <label for="tipo">Tipo de Estabelecimento</label>
              <input type="text" name="tipo" id="tipo" class="form-control form-control-sm" value="{{$estabelecimento->tipo}}" disabled>
          </div>
          <div class="form-group col-md-4">
              <label for="subtipo">Subtipo de Estabelecimento</label>
              <input type="text" name="subtipo" id="subtipo" class="form-control form-control-sm" value="{{$estabelecimento->subtipo}}" disabled>
          </div>
          @foreach ($estabelecimento->users as $user)
            @if ($user->admin == 1)
              <div class="form-group col-md-4">
                  <label for="subtipo">Administrador</label>
                  <input type="text" class="form-control form-control-sm" value="{{$user->name}}" disabled>
              </div>
            @endif
          @endforeach
      </div>
  </div>
</div>
@endsection
