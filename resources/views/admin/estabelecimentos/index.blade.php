@extends('admin.layouts.basemasksearch')

@section('content')
<div class="container">
  <div class="top-left mx-3 my-3">
    <div class="btn-toolbar" role="toolbar">
        <div class="btn-group" role="group">
            <a href="{{action('Admin\EstabelecimentoController@create')}}" class="btn btn-success rounded btn-sm" data-toggle="tooltip" data-placement="bottom" title="Cadastrar Estabelecimento">Cadastrar</a>
        </div>
    </div>
  </div>
<form action="{{action('Admin\EstabelecimentoController@index')}}">
  {{ csrf_field() }}
  <nav class="navbar navbar-light bg-light mx-3 my-3">
    <input class="form-control mr-sm-2 col-md-6" name="search_estab" type="search" placeholder="Consultar por Nome ou CNES ou CNPJ ou Gestão ou N. Jurídica" aria-label="Search">
  </nav>
</form>

  <table class="table table-bordered table-responsive-sm table-hover table-sm text-center">
    <thead>
      <tr>
        <th scope="col" style="width: 10%;">Estabelecimento</th>
        <th scope="col" style="width: 5%;">CNES</th>
        <th scope="col" style="width: 10%;">Natureza Jurídica</th>
        <th scope="col" style="width: 10%;">Empresarial</th>
        <th scope="col" style="width: 10%;">CNPJ</th>
        <th scope="col" style="width: 10%;">Gestão</th>
        <th scope="col" style="width: 10%;">Administrador</th>
        <th scope="col" style="width: 10%">Opções</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($estabelecimentos as $estabelecimento)
      <tr>
        <td>{{$estabelecimento->nome}}</td>
        <td>{{$estabelecimento->cnes}}</td>
        <td>{{$estabelecimento->natureza_juridica}}</td>
        <td>{{$estabelecimento->nome_empresarial}}</td>
        <td>{{$estabelecimento->cnpj}}</td>
        <td>{{$estabelecimento->gestao}}</td>
        <td>
        @foreach ($estabelecimento->users as $user)
          @if ($user->admin == 1)
          {{$user->name}}
          @else

          @endif
        @endforeach
        </td>
        <td>
          <a href="{{action('Admin\EstabelecimentoController@show', $estabelecimento->id)}}" class="btn btn-outline-success rounded btn-sm mb-1">Visualizar</a>
          <a href="{{action('Admin\EstabelecimentoController@edit', $estabelecimento->id)}}" class="btn btn-outline-secondary rounded btn-sm mb-1">Editar</a>
          <form class="" action="{{action('Admin\EstabelecimentoController@destroy', $estabelecimento->id)}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-outline-danger rounded btn-sm mb-1">Deletar</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
</table>
<div class="text-center">
  {{$estabelecimentos->appends(Request::only('search_estab'))->render("pagination::bootstrap-4")}}
</div>
</div>
@endsection
