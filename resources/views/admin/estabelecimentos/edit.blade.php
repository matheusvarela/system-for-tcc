@extends('admin.layouts.base')

@section('content')
<div class="container">
  <form class="" action="{{action('Admin\EstabelecimentoController@update', $estabelecimento->id)}}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">

    <div class="bg-light border-left border-bottom rounded" id="dados_estabelecimento">
        <h3 class="border-gray border-bottom mx-3 my-3">Editar dados do Estabelecimento</h3>
        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }} col-md-6">
                <label for="nome">Nome</label>
                <input type="text" name="nome" class="form-control form-control-sm" id="nome" placeholder="Nome do estabelecimento" value="{{ $estabelecimento->nome or old('nome')}}" required >

                @if ($errors->has('nome'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('cnes') ? ' has-error' : '' }} col-md-2">
                <label for="cnes">CNES</label>
                <input type="text" name="cnes" class="form-control form-control-sm" id="cnes" placeholder="CNES" value="{{$estabelecimento->cnes or old('cnes')}}" required>

                @if ($errors->has('cnes'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cnes') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }} col-md-3">
                <label for="cnpj">CNPJ</label>
                <input type="text" name="cnpj" class="form-control form-control-sm" id="cnpj" placeholder="CNPJ" value="{{$estabelecimento->cnpj or old('cnpj')}}">
                @if ($errors->has('cnpj'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cnpj') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('nome_empresarial') ? ' has-error' : '' }} col-md-6">
                <label for="nome_empresarial">Nome Empresarial</label>
                <input type="text" name="nome_empresarial" class="form-control form-control-sm" id="nome_empresarial" placeholder="Nome Empresarial" value="{{$estabelecimento->nome_empresarial or old('nome_empresarial')}}" required>
                @if ($errors->has('nome_empresarial'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nome_empresarial') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('natureza_juridica') ? ' has-error' : '' }} col-md-5">
                <label for="cnes">Natureza Jurídica (Grupo)</label>
                <input type="text" name="natureza_juridica" class="form-control form-control-sm" id="natureza_juridica" placeholder="Natureza Jurídica" value="{{$estabelecimento->natureza_juridica or old('natureza_juridica')}}" required>

                @if ($errors->has('natureza_juridica'))
                    <span class="help-block">
                        <strong>{{ $errors->first('natureza_juridica') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('logradouro') ? ' has-error' : '' }} col-md-6">
                <label for="logradouro">logradouro</label>
                <input type="text" name="logradouro" class="form-control form-control-sm" id="logradouro" placeholder="Logradouro" value="{{$estabelecimento->logradouro or old('logradouro')}}" required>

                @if ($errors->has('logradouro'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logradouro') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }} col-md-2">
                <label for="numero">Número</label>
                <input type="text" name="numero" class="form-control form-control-sm" value="{{$estabelecimento->numero or old('numero')}}" id="numero">

                @if ($errors->has('numero'))
                    <span class="help-block">
                        <strong>{{ $errors->first('numero') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('complemento') ? ' has-error' : '' }} col-md-3">
                <label for="complemento">Complemento</label>
                <input type="text" name="complemento" class="form-control form-control-sm" value="{{$estabelecimento->complemento or old('complemento')}}" id="complemento">

                @if ($errors->has('complemento'))
                    <span class="help-block">
                        <strong>{{$errors->first('complemento')}}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('bairro') ? ' has-error' : '' }} col-md-4">
                <label for="bairro">Bairro</label>
                <input name="bairro" id="bairro" class="form-control form-control-sm" value="{{$estabelecimento->bairro or old('bairro')}}" required>

                @if ($errors->has('bairro'))
                    <span class="help-block">
                      <strong>{{$errors->first('bairro')}}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('municipio') ? ' has-error' : '' }} col-md-4">
                <label for="municipio">Município</label>
                <input name="municipio" id="municipio" class="form-control form-control-sm" value="{{$estabelecimento->municipio or old('municipio')}}" required>

                @if ($errors->has('municipio'))
                  <span class="help-block">
                      <strong>{{$errors->first('municipio')}}</strong>
                  </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('uf') ? ' has-error' : '' }} col-md-2">
                <label for="uf">UF</label>
                <input name="uf" id="uf" class="form-control form-control-sm" maxlength="2" value="{{$estabelecimento->uf or old('uf')}}" required>

                @if ($errors->has('uf'))
                  <span class="help-block">
                      <strong>{{$errors->first('uf')}}</strong>
                  </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('cep') ? ' has-error' : '' }} col-md-2">
                <label for="cep">CEP</label>
                <input type="text" name="cep" id="cep" class="form-control form-control-sm" value="{{$estabelecimento->cep or old('cep')}}" maxlength="8" required>

                @if ($errors->has('cep'))
                  <span class="help-block">
                      <strong>{{$errors->first('cep')}}</strong>
                  </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('telefone') ? ' has-error' : '' }} col-md-2">
                <label for="telefone">Telefone</label>
                <input type="text" name="telefone" id="telefone" class="form-control form-control-sm" value="{{$estabelecimento->telefone or old('telefone')}}" maxlength="14">
                @if ($errors->has('telefone'))
                  <span class="help-block">
                      <strong>{{$errors->first('telefone')}}</strong>
                  </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('dependencia') ? ' has-error' : '' }} col-md-2">
                <label for="dependencia">Dependência</label>
                <input type="text" name="dependencia" id="dependencia" class="form-control form-control-sm" value="{{$estabelecimento->dependencia or old('dependencia')}}" required>
                @if ($errors->has('dependencia'))
                  <span class="help-block">
                      <strong>{{$errors->first('dependencia')}}</strong>
                  </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('regional_saude') ? ' has-error' : '' }} col-md-2">
                <label for="regional_saude">Regional de Saúde</label>
                <input type="text" name="regional_saude" id="regional_saude" value="{{$estabelecimento->regional_saude or old('regional_saude')}}" class="form-control form-control-sm">
                @if ($errors->has('regional_saude'))
                  <span class="help-block">
                      <strong>{{$errors->first('regional_saude')}}</strong>
                  </span>
                @endif
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }} col-md-4">
                <label for="tipo">Tipo de Estabelecimento</label>
                <input type="text" name="tipo" id="tipo" class="form-control form-control-sm" value="{{$estabelecimento->tipo or old('tipo')}}" required>

                @if ($errors->has('tipo'))
                  <span class="help-block">
                      <strong>{{$errors->first('tipo')}}</strong>
                  </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('subtipo') ? ' has-error' : '' }} col-md-4">
                <label for="subtipo">Subtipo de Estabelecimento</label>
                <input type="text" name="subtipo" id="subtipo" class="form-control form-control-sm" value="{{$estabelecimento->subtipo or old('subtipo')}}">

                @if ($errors->has('subtipo'))
                  <span class="help-block">
                      <strong>{{$errors->first('subtipo')}}</strong>
                  </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('gestao') ? ' has-error' : '' }} col-md-2">
                <label for="gestao">Gestão</label>
                <input type="text" name="gestao" id="gestao" class="form-control form-control-sm" value="{{$estabelecimento->gestao or old('gestao')}}" required>

                @if ($errors->has('gestao'))
                  <span class="help-block">
                      <strong>{{$errors->first('gestao')}}</strong>
                  </span>
                @endif
            </div>
        </div>
    </div>

    <div class="form-group mx-3 my-3">
      <button type="submit" class="btn btn-secondary btn-sm"  name="atualizar">Atualizar</button>
    </div>

  </form>
</div>
@endsection
