@extends('admin.layouts.basemasksearch')
@section('content')
<div class="container">
  <div class="bg-light border-left border-bottom rounded" id="dados_estabelecimento">
      <h3 class="border-gray border-bottom mx-3 my-3">Dados do funcionário</h3>
      <div class="form-row mx-3">
          <div class="form-group col-md-6">
            <label for="name" class="control-label">Nome</label>
                <input id="name" type="text" class="form-control form-control-sm" name="name" value="{{$funcionario->name}}" disabled>
          </div>

          <div class="form-group col-md-6">
              <label for="email" class="control-label">Email</label>
                  <input id="email" type="email" class="form-control form-control-sm" name="email" value="{{$funcionario->email}}" disabled>
          </div>
      </div>

      <div class="form-row mx-3">
          <div class="form-group col-md-3">
              <label for="cpf" class="control-label">CPF</label>
                  <input id="cpf" name="cpf" type="tel" class="form-control form-control-sm" value="{{ $funcionario->cpf}}" disabled>
          </div>

          <div class="form-group col-md-3">
              <label for="matricula" class="control-label">Matrícula</label>
                  <input id="matricula" type="text" name="matricula" class="form-control form-control-sm" value="{{ $funcionario->matricula }}" disabled>
          </div>

          <div class="form-group col-md-4">
              <label for="cargo" class="control-label">Cargo</label>
                  <input id="cargo" type="text" name="cargo" class="form-control form-control-sm" value="{{ $funcionario->cargo }}" disabled>
          </div>
      </div>

      <div class="form-row mx-3">
          <div class="form-group col-md-6">
              <label for="funcao" class="control-label">Função</label>
                  <input id="funcao" type="text" name="funcao" class="form-control form-control-sm" value="{{ $funcionario->funcao }}" disabled>
          </div>
          <div class="form-group col-md-6">
              <label for="estabelecimento">Estabelecimento</label>
              <input type="text" name="estabelecimento" id="estabelecimento" class="form-control form-control-sm" value="{{$funcionario->estabelecimentos->nome}}" disabled>
          </div>
      </div>

      <div class="form-row mx-3">
        <div class="form-group col-md-6">
          <label for="password" class="control-label">Senha</label>
            <input id="password" type="password" class="form-control form-control-sm" name="password" value="{{$funcionario->password}}" disabled>
        </div>

        <div class="form-group col-md-6">
          <label for="password-confirm" class="control-label">Confirme a senha</label>
            <input id="password-confirm" type="password" class="form-control form-control-sm" name="password_confirmation" value="{{$funcionario->password}}" disabled>
        </div>

      </div>
  </div>
</div>
@endsection
