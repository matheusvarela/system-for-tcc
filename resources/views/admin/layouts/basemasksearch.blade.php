<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>GPSM</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="system">
      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom border-gray">
        <a class="navbar-brand" href="{{action('Admin\HomeController@index')}}">GPSM</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav mr-auto">
            &nbsp;
          </ul>

          <ul class="nav navbar-nav navbar-right">

            <!-- Authentication Links -->
            @if (!Auth::guard('admin')->check())
              <!--<li><a href="{{ route('admin.login') }}" class="nav-link">Login</a></li>-->
              <li><a href="{{action('HomeController@index')}}" class="nav-link">Área de usuários</a></li>
            @else
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                  <span>Bem vindo, {{ Auth::guard('admin')->user()->name }}</span>

                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" role="menu">
                  <li><a href="{{action('Admin\EstabelecimentoController@index')}}" class="dropdown-item">Área Estabelecimentos</a></li>
                  <li><a href="{{action('Admin\AdmEstabelecimentoController@index')}}" class="dropdown-item">Área Funcionários</a></li>
                  <li>
                      <a class="dropdown-item" href="{{ route('admin.logout') }}"
                          onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                              Sair
                      </a>

                      <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                    </li>
                  </ul>
                </li>
                @endif
              </ul>
            </div>
          </nav>
          @yield('content')
        </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.14/jquery.mask.js"></script>

</body>
</html>
