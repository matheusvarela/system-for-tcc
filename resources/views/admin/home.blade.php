@extends('admin.layouts.base')

@section('content')
<div class="container">
    <div class="row align-items-center justify-content-left mt-5 mx-3">
        <div class="card-deck">
            <div class="card bg-light mb-3" style="max-width: 18rem;">
                <div class="card-header">Estabelecimentos</div>
                    <div class="card-body">
                        <h5 class="card-title">Área Estabelecimentos</h5>
                            <p class="card-text">Acesso a área dos estabelecimentos.</p>
                                <a href="{{action('Admin\EstabelecimentoController@index')}}" class="btn btn-primary">Avançar</a>
                    </div>
            </div>


            <div class="card bg-light mb-3" style="max-width: 18rem;">
                <div class="card-header">Funcionários</div>
                    <div class="card-body">
                        <h5 class="card-title">Área Funcionários</h5>
                            <p class="card-text">Acesso a área dos funcionários.</p>
                                <a href="{{action('Admin\AdmEstabelecimentoController@index')}}" class="btn btn-primary">Avançar</a>
                    </div>
            </div>


        </div>
    </div>
</div>
@endsection
