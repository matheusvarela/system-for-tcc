@extends('layouts.base')

@section('content')
<div class="container">
    <div class="bg-light border-left border-bottom rounded">
        <h3 class="border-gray border-bottom my-3 mx-3">Informações do paciente</h3>

        <div class="form-row mx-3">
            <div class="form-group col-md-8">
                <label for="">Paciente</label>
                <input type="text" readonly class="form-control form-control-sm" value="{{$evolucao->triagems->pacientes->nome}}" disabled>
            </div>

            <div class="form-group col-md-2">
                <label for="">Data de nascimento</label>
                <input type="text" value="{{$evolucao->triagems->pacientes->data_nascimento}}" disabled class="form-control form-control-sm">
            </div>

            <div class="form-group col-md-2">
                <label for="">Sexo</label>
                <input type="text" name="" value="{{$evolucao->triagems->pacientes->sexo}}" disabled class="form-control form-control-sm">
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group col-md-8">
                <label for="">Endereço</label>
                <input type="text" value="{{$evolucao->triagems->pacientes->endereco}}" disabled class="form-control form-control-sm">
            </div>

            <div class="form-group col-md-2">
                <label for="">Telefone</label>
                <input type="text" name="" value="{{$evolucao->triagems->pacientes->telefone}}" id="telefone" disabled class="form-control form-control-sm">
            </div>
        </div>
    </div>
        <div class="bg-light border-left border-bottom rounded">
            <h3 class="border-gray border-bottom my-3 mx-3">Ficha evolutiva</h3>


                <div class="form-row mx-3">
                    <div class="form-group col-md-8">
                        <label for="rubrica">Responsável pela evolução</label>
                        <input type="text" class="form-control form-control-sm" value="{{$evolucao->users->name}}" disabled>
                    </div>

                    <div class="form-group col-md-2">
                        <label for="data">Data</label>
                        <input class="form-control form-control-sm" value="{{date('d/m/Y', strtotime($evolucao->created_at))}}" disabled>
                    </div>
                </div>

                <div class="form-row mx-3">
                    <div class="form-group col-md-6">
                        <label for="historico">Histórico</label>
                        <textarea rows="3" disabled class="form-control form-control-sm">{{$evolucao->historico}}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="diagnostico">Diagnóstico</label>
                        <textarea  rows="3" class="form-control form-control-sm" disabled>{{$evolucao->diagnostico}}</textarea>
                    </div>
                </div>

                <div class="form-row mx-3">
                    <div class="form-group col-md-6">
                        <label for="conduta">Conduta</label>
                        <textarea rows="3" class="form-control form-control-sm" disabled>{{$evolucao->conduta}}</textarea>
                    </div>
                </div>
        </div>
</div>

@endsection
