@extends('layouts.base')

@section('content')
<div class="container">
  @if ($evolucoes->count() > 0)
  <div class="top-left mx-3 my-3">
      <div class="btn-toolbar" role="toolbar">
          <div class="btn-group" role="group">
              <a href="{{action('EvolucaoController@create', $triagems->last()->id)}}" class="btn btn-success rounded btn-sm" data-toggle="tooltip" data-placement="bottom" title="Cadastrar evolução">Cadastrar</a>
          </div>
      </div>
  </div>
  <table class="table table-bordered table-responsive-sm table-hover table-sm text-center">
    <thead>
      <tr>
        <th scope="col" style="width: 15%;">Responsável da Evolução</th>
        <th scope="col" style="width: 15%;">Paciente</th>
        <th scope="col">Histórico</th>
        <th scope="col">Diagnóstico</th>
        <th scope="col">Conduta</th>
        <th scope="col">Criada</th>
        <th scope="col" style="width: 10%;">Relacionada a Triagem</th>
        <th scope="col">Opções</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($evolucoes as $evolucao)
        <tr>
          <td>{{$evolucao->users->name}}</td>
          <td>{{$evolucao->triagems->pacientes->nome}}</td>
          <td>{{$evolucao->historico}}</td>
          <td>{{$evolucao->diagnostico}}</td>
          <td>{{$evolucao->conduta}}</td>
          <td>{{date('d/m/Y', strtotime($evolucao->created_at))}}</td>
          <td><a href="{{action('TriagemController@show', $evolucao->triagems->id)}}" class="btn btn-outline-success btn-sm rounded">Visualizar</a></td>
          <td>
            <a href="{{action('EvolucaoController@show', $evolucao->id)}}" class="btn btn-outline-success btn-sm rounded mb-1">Visualizar</a>
            <a href="{{action('EvolucaoController@edit', $evolucao->id)}}" class="btn btn-outline-secondary btn-sm rounded mb-1">Editar</a>
            <form action="{{action('EvolucaoController@destroy', $evolucao->id)}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-outline-danger rounded btn-sm">Deletar</button>
            </form>
          </td>
        </tr>
        @endforeach
    </tbody>
  </table>

  <div class="text-center">
    {{$evolucoes->render("pagination::bootstrap-4")}}
  </div>

  @else
  <div class="alert alert-danger mx-3 my-3 col-md-6" role="alert">
      O paciente não possui triagem cadastrada, com isso está impossibilitado de obter a evolução!
  </div>
  @endif

</div>
@endsection
