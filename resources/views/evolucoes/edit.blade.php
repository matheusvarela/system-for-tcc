@extends('layouts.base')

@section('content')

<div class="container">
    <div class="bg-light border-left border-bottom rounded">
        <h3 class="border-gray border-bottom my-3 mx-3">Informações do paciente</h3>

        <div class="form-row mx-3">
            <div class="form-group col-md-8">
                <label for="">Paciente</label>
                <input type="text" value="{{$evolucao->triagems->pacientes->nome}}" readonly class="form-control form-control-sm">
            </div>

            <div class="form-group col-md-2">
                <label for="">Data de nascimento</label>
                <input type="text" value="{{$evolucao->triagems->pacientes->data_nascimento}}" readonly class="form-control form-control-sm">
            </div>

            <div class="form-group col-md-2">
                <label for="">Sexo</label>
                <input type="text" name="" value="{{$evolucao->triagems->pacientes->sexo}}" readonly class="form-control form-control-sm">
            </div>
        </div>

        <div class="form-row mx-3">
            <div class="form-group col-md-8">
                <label for="">Endereço</label>
                <input type="text" value="{{$evolucao->triagems->pacientes->endereco}}" readonly class="form-control form-control-sm">
            </div>

            <div class="form-group col-md-2">
                <label for="">Telefone</label>
                <input type="text" name="" value="{{$evolucao->triagems->pacientes->telefone}}" id="telefone" readonly class="form-control form-control-sm">
            </div>
        </div>
    </div>
    <form class="" action="{{ action('EvolucaoController@update', $evolucao->id) }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PATCH">
        <div class="bg-light border-left border-bottom rounded">
            <h3 class="border-gray border-bottom my-3 mx-3">Ficha evolutiva</h3>


                <div class="form-row mx-3">
                    <div class="form-group col-md-8">
                        <label for="rubrica">Responsável pela evolução</label>
                        <input type="text" name="rubrica" id="rubrica" class="form-control form-control-sm" value="{{$evolucao->users->name}}" readonly>
                    </div>

                    <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }} col-md-2">
                        <label for="data">Data</label>
                        <input name="data" id="data" class="form-control form-control-sm" value="{{$evolucao->data}}" readonly>

                        @if ($errors->has('data'))
                            <span class="help-block">
                                <strong>{{ $errors->first('data') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-row mx-3">
                    <div class="form-group{{ $errors->has('historico') ? ' has-error' : '' }} col-md-6">
                        <label for="historico">Histórico*</label>
                        <textarea name="historico" id="historico" rows="3" required class="form-control form-control-sm">{{ old('historico') ? old('historico') : $evolucao->historico}}</textarea>

                        @if ($errors->has('historico'))
                            <span class="help-block">
                                <strong>{{ $errors->first('historico') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('diagnostico') ? ' has-error' : '' }} col-md-6">
                        <label for="diagnostico">Diagnóstico*</label>
                        <textarea name="diagnostico" id="diagnostico" rows="3" required class="form-control form-control-sm">{{ old('diagnostico') ? old('diagnostico') : $evolucao->diagnostico}}</textarea>

                        @if ($errors->has('diagnostico'))
                            <span class="help-block">
                                <strong>{{ $errors->first('diagnostico') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-row mx-3">
                    <div class="form-group col-md-6">
                        <label for="conduta">Conduta*</label>
                        <textarea name="conduta" rows="3" id="conduta" required class="form-control form-control-sm">{{ old('conduta') ? old('conduta') : $evolucao->conduta}}</textarea>

                        @if ($errors->has('conduta'))
                            <span class="help-block">
                                <strong>{{ $errors->first('conduta') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
        </div>

        <div class="form-group mx-3 my-3">
          <button type="submit" class="btn btn-secondary btn-sm" data-toggle="tooltip" data-placement="bottom" title="Atualizar ficha evolutiva" name="cadastrar">Atualizar</button>
        </div>
    </form>
</div>

@endsection
